package pl.matflis.parking.syncdb.provider;

public interface PropertiesProvider {
	String getJdbcUrl();

	String getDriverClass();

	String getUsername();

	String getPassword();
}