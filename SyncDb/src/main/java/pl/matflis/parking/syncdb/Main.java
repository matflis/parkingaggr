package pl.matflis.parking.syncdb;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main {

	public static void main(String[] args) {
		DbUpdateTask updateTask = new DbUpdateTask();
		updateTask.initState();
		ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
		executorService.scheduleWithFixedDelay(updateTask::doUpdate, 0, 5, TimeUnit.SECONDS);
	}
}