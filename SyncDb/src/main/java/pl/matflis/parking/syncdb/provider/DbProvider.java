package pl.matflis.parking.syncdb.provider;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sql2o.Sql2o;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;

public final class DbProvider {

	private static final Logger logger = LoggerFactory.getLogger(DbProvider.class);

	private static Sql2o parkingWebMobileDbConnection;
	private static Sql2o parkingWebViewerDbConnection;

	public static Sql2o getParkingWebDbConnector() {
		if (parkingWebMobileDbConnection == null) {
			logger.info("Initializing ParkingMobile db connection");
			try {
				parkingWebMobileDbConnection = new Sql2o(getDataSource(ParkingWebPropertiesSource.getInstance()));
				logger.info("ParkingMobile Db connection initialized");
			} catch (PropertyVetoException e) {
				logger.error("Cannot initialize ParkingMobile connection. Reason: " + e.getMessage(), e);
				throw new DbConnectionInitializationException("ParkingMobile Db connection initialization failed");
			}
		}
		return parkingWebMobileDbConnection;
	}

	public static Sql2o getParkingViewerDbConnector() {
		if (parkingWebViewerDbConnection == null) {
			logger.info("Initializing ParkingViewer db connection");
			try {
				parkingWebViewerDbConnection = new Sql2o(getDataSource(ParkingViewerPropertiesSource.getInstance()));
				logger.info("ParkingViewer Db connection initialized");
			} catch (PropertyVetoException e) {
				logger.error("Cannot initialize ParkingViewer connection. Reason: " + e.getMessage(), e);
				throw new DbConnectionInitializationException("ParkingViewer Db connection initialization failed");
			}
		}
		return parkingWebViewerDbConnection;
	}

	private static DataSource getDataSource(PropertiesProvider propertiesProvider) throws PropertyVetoException {
		ComboPooledDataSource dataSource = new ComboPooledDataSource();
		dataSource.setDriverClass(propertiesProvider.getDriverClass());
		dataSource.setJdbcUrl(propertiesProvider.getJdbcUrl());
		dataSource.setUser(propertiesProvider.getUsername());
		dataSource.setPassword(propertiesProvider.getPassword());
		return dataSource;
	}

	private static class DbConnectionInitializationException extends RuntimeException {
		DbConnectionInitializationException(String message) {
			super(message);
		}
	}
}