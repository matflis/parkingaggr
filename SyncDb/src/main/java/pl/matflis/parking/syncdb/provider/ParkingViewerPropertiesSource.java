package pl.matflis.parking.syncdb.provider;

public class ParkingViewerPropertiesSource implements PropertiesProvider {

	private static final ParkingViewerPropertiesSource Instance = new ParkingViewerPropertiesSource();

	// TODO: FILL THOSE PROPERTIES
	static ParkingViewerPropertiesSource getInstance() {
		return Instance;
	}

	private ParkingViewerPropertiesSource() {
	}

	@Override
	public String getJdbcUrl() {
		return "jdbc:mysql://localhost:3306/parkingviewerweb?serverTimezone=UTC";
	}

	@Override
	public String getDriverClass() {
		return "com.mysql.jdbc.Driver";
	}

	@Override
	public String getUsername() {
		return "root";
	}

	@Override
	public String getPassword() {
		return "";
	}
}