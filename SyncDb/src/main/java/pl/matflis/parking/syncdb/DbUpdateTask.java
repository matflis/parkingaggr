package pl.matflis.parking.syncdb;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sql2o.Connection;
import org.sql2o.Query;
import org.sql2o.ResultSetHandler;
import org.sql2o.Sql2o;
import pl.matflis.parking.syncdb.provider.DbProvider;
import pl.matflis.parkingweb.core.common.Position;
import pl.matflis.parkingweb.core.subscriptions.model.CountryCode;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionDTO;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionDTO.SubscriptionDTOBuilder;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionEntryInfo;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionSubmissionDTO;
import pl.matflis.parkingweb.core.subscriptions.model.ViolationType;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

final class DbUpdateTask {

	private static final Logger logger = LoggerFactory.getLogger(DbUpdateTask.class);
	private static final String SELECT_MAX_QUERY = "SELECT MAX(OffenceTimestamp) FROM Charges";

	private final Sql2o webDbConnector;
	private final Sql2o viewerDbConnector;

	private Long lastChargesParkingWebTimestamp;
	private Long lastChargesParkingWebViewerTimestamp;

	DbUpdateTask() {
		this.webDbConnector = DbProvider.getParkingWebDbConnector();
		this.viewerDbConnector = DbProvider.getParkingViewerDbConnector();
	}

	void initState() {
		// Select max from first
		lastChargesParkingWebTimestamp = getMaxTimestamp(webDbConnector);
		logger.info("Last timestamp record of ParkingWeb app is '{}'", lastChargesParkingWebTimestamp);
		// Select max from second
		lastChargesParkingWebViewerTimestamp = getMaxTimestamp(viewerDbConnector);
		logger.info("Last timestamp record of ParkingViewer app is '{}'", lastChargesParkingWebViewerTimestamp);
	}

	void doUpdate() {
		lastChargesParkingWebTimestamp = getMaxTimestamp(webDbConnector);
		lastChargesParkingWebViewerTimestamp = getMaxTimestamp(viewerDbConnector);

		if (lastChargesParkingWebTimestamp > lastChargesParkingWebViewerTimestamp) {
			logger.info("Update needed. Last ParkingWeb is {} while ParkingViewer is {}",
					lastChargesParkingWebTimestamp, lastChargesParkingWebViewerTimestamp);
			transferRows(lastChargesParkingWebViewerTimestamp);
		} else {
			logger.info("No need to update. Both db's are synced");
		}
	}

	private void transferRows(Long lastTimestamp) {
		List<SubscriptionSubmissionDTO> missingEntries = getMissingEntries(lastTimestamp);
		logger.debug("Need to transfer {} rows", missingEntries.size());
		missingEntries.forEach(this::insertEntry);
		logger.debug("Transfer completed successfully");
	}

	private List<SubscriptionSubmissionDTO> getMissingEntries(Long lastTimestamp) {
		String upperThanQuery = "SELECT u.Username, c.Longitude, c.Latitude, c.OffenceTimestamp, c.StreetName, " +
				"c.RegistrationPlate, c.CountryCode, c.Brand, c.Remarks, c.ViolationType, c.TicketAmount" +
				" FROM Charges c INNER JOIN Users u ON c.UserId = u.ID WHERE OffenceTimestamp > :lastTimestamp";
		try (Connection connection = webDbConnector.open()) {
			return connection.createQuery(upperThanQuery)
					.addParameter("lastTimestamp", lastTimestamp)
					.executeAndFetch((ResultSetHandler<SubscriptionSubmissionDTO>) resultSet -> {
						String user = resultSet.getString("Username");
						Position position = toPosition(resultSet);
						SubscriptionEntryInfo subscriptionEntry = toSubscriptionEntry(resultSet);
						return new SubscriptionSubmissionDTO(user, subscriptionEntry, position);
					});
		}
	}

	private Position toPosition(ResultSet resultSet) throws SQLException {
		return new Position(resultSet.getDouble("Longitude"), resultSet.getDouble("Latitude"));
	}

	private SubscriptionEntryInfo toSubscriptionEntry(ResultSet resultSet) throws SQLException {
		SubscriptionDTO subscriptionDto = new SubscriptionDTOBuilder()
				.withStreetName(resultSet.getString("StreetName"))
				.withRegistrationNumber(resultSet.getString("RegistrationPlate"))
				.withCountryCode(CountryCode.fromString(resultSet.getString("CountryCode")))
				.withBrand(resultSet.getString("Brand"))
				.withRemarks(resultSet.getString("Remarks"))
				.withViolationType(ViolationType.fromDescription(resultSet.getString("ViolationType")))
				.withTicketAmount(resultSet.getInt("TicketAmount"))
				.build();
		DateTime offenceTime = new DateTime(resultSet.getLong("OffenceTimestamp"));
		return new SubscriptionEntryInfo(subscriptionDto, null, offenceTime);
	}

	private void insertEntry(SubscriptionSubmissionDTO entry) {
		Position position = entry.getPosition();
		SubscriptionEntryInfo subscriptionEntry = entry.getSubscriptionEntryInfo();
		SubscriptionDTO subscriptionDTO = subscriptionEntry.getSubscriptionDTO();

		try (Connection connection = viewerDbConnector.beginTransaction()) {
			Query insertQuery = connection.createQuery("INSERT INTO Charges (UserId, Longitude, Latitude, OffenceTimestamp, " +
					"StreetName, RegistrationPlate, CountryCode, Brand, Remarks, ViolationType, TicketAmount) VALUES (" +
					"(SELECT ID FROM Users WHERE Username = :username)," + // Username
					":longitude, " +
					":latitude, " +
					":timestamp, " +
					":street, " +
					":plate, " +
					":country, " +
					":brand, " +
					":remarks, " +
					":violation, " +
					":amount);");
			insertQuery.addParameter("username", entry.getUserName());
			insertQuery.addParameter("longitude", position.getLongitude());
			insertQuery.addParameter("latitude", position.getLatitude());
			insertQuery.addParameter("timestamp", subscriptionEntry.getOffenceTime().getMillis());
			insertQuery.addParameter("street", subscriptionDTO.getStreetName());
			insertQuery.addParameter("plate", subscriptionDTO.getRegistrationNumber());
			insertQuery.addParameter("country", subscriptionDTO.getCountryCode().name());
			insertQuery.addParameter("brand", subscriptionDTO.getBrand());
			insertQuery.addParameter("remarks", subscriptionDTO.getRemarks());
			insertQuery.addParameter("violation", subscriptionDTO.getViolationType().name());
			insertQuery.addParameter("amount", subscriptionDTO.getTicketAmount());
			insertQuery.executeUpdate();
			connection.commit();
		}
	}

	private Long getMaxTimestamp(Sql2o sql2o) {
		try (Connection connection = sql2o.open()) {
			return Optional.ofNullable(connection.createQuery(SELECT_MAX_QUERY)
					.executeAndFetchFirst(Long.class))
					.orElse(0L);
		}
	}

	Long getLastChargesParkingWebTimestamp() {
		return lastChargesParkingWebTimestamp;
	}

	Long getLastChargesParkingWebViewerTimestamp() {
		return lastChargesParkingWebViewerTimestamp;
	}
}