package pl.matflis.parking.syncdb.provider;

public class ParkingWebPropertiesSource implements PropertiesProvider {

	private static final ParkingWebPropertiesSource Instance = new ParkingWebPropertiesSource();

	// TODO: FILL THOSE PROPERTIES
	static ParkingWebPropertiesSource getInstance() {
		return Instance;
	}

	private ParkingWebPropertiesSource() {
	}

	@Override
	public String getJdbcUrl() {
		return "jdbc:mysql://localhost:3306/parkingweb?serverTimezone=UTC";
	}

	@Override
	public String getDriverClass() {
		return "com.mysql.jdbc.Driver";
	}

	@Override
	public String getUsername() {
		return "root";
	}

	@Override
	public String getPassword() {
		return "";
	}
}