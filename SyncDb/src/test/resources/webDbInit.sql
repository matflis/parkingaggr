CREATE TABLE IF NOT EXISTS Users (
  ID       BIGINT PRIMARY KEY AUTO_INCREMENT,
  Username VARCHAR(100) NOT NULL UNIQUE,
  Password VARCHAR(100) NOT NULL
);
CREATE TABLE IF NOT EXISTS Charges (
  ID                BIGINT PRIMARY KEY AUTO_INCREMENT,
  UserID            INT          NOT NULL,
  Longitude         DOUBLE       NOT NULL,
  Latitude          DOUBLE       NOT NULL,
  OffenceTimestamp  BIGINT       NOT NULL,
  StreetName        VARCHAR(100) NOT NULL,
  RegistrationPlate VARCHAR(100) NOT NULL,
  CountryCode       VARCHAR(10)  NOT NULL,
  Brand             VARCHAR(100) NOT NULL,
  Remarks           VARCHAR(100),
  ViolationType     VARCHAR(100) NOT NULL,
  TicketAmount      INT          NOT NULL,
  FOREIGN KEY (UserID) REFERENCES Users (ID),
  CHECK TicketAmount > 0,
  CHECK OffenceTimestamp > 0
);
-- Users content
INSERT INTO Users (Username, Password) VALUES ('admin', 'admin');

-- Charges content
INSERT INTO Charges (UserID, Longitude, Latitude, OffenceTimestamp, StreetName, RegistrationPlate, CountryCode, Brand, Remarks, ViolationType, TicketAmount)
VALUES (SELECT ID
        FROM Users
        WHERE Username = 'admin', 10, 10, 150000000, 'Nosowicka', 'TD 123', 'PL', 'Toyota', 'Brak', 'LACK_OF_TICKET',
                                  100);
INSERT INTO Charges (UserID, Longitude, Latitude, OffenceTimestamp, StreetName, RegistrationPlate, CountryCode, Brand, Remarks, ViolationType, TicketAmount)
VALUES (SELECT ID
        FROM Users
        WHERE Username = 'admin', 10, 12, 150000300, 'Nosowicka', 'TD 123', 'PL', 'Toyota', NULL, 'LACK_OF_TICKET',
                                  101);