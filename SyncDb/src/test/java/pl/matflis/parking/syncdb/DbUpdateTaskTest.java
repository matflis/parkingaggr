package pl.matflis.parking.syncdb;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.sql2o.Sql2o;
import pl.matflis.parking.syncdb.provider.DbProvider;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.io.File;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
@PrepareForTest(DbProvider.class)
public class DbUpdateTaskTest {

	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Before
	public void init() throws Exception {
		mockStatic(DbProvider.class);

		File webDbFile = temporaryFolder.newFile();
		File viewerDbFile = temporaryFolder.newFile();
		when(DbProvider.getParkingWebDbConnector()).thenReturn(new Sql2o(getDataSource(webDbFile)));
		when(DbProvider.getParkingViewerDbConnector()).thenReturn(new Sql2o(getDataSource(viewerDbFile)));

		loadScript("/viewerDbInit.sql", viewerDbFile);
		loadScript("/webDbInit.sql", webDbFile);
	}

	private DataSource getDataSource(File webDbFile) throws PropertyVetoException {
		ComboPooledDataSource dataSource = new ComboPooledDataSource();
		dataSource.setDriverClass("org.h2.Driver");
		dataSource.setJdbcUrl(getJdbcUrl(webDbFile));
		dataSource.setUser("root");
		dataSource.setPassword("sa");
		return dataSource;
	}

	private String getJdbcUrl(File webDbFile) {
		return "jdbc:h2:file:" + webDbFile.getAbsolutePath();
	}

	private void loadScript(String resourcePath, File dbFile) throws Exception {
		try (Connection connection = DriverManager.getConnection(getJdbcUrl(dbFile), "root", "sa");
			 Statement stmt = connection.createStatement()) {
			stmt.addBatch(IOUtils.toString(getClass().getResourceAsStream(resourcePath), Charset.defaultCharset()));
			stmt.executeBatch();
			connection.commit();
		}
	}

	@Test
	public void shouldPerformIntegration() {
		DbUpdateTask updateTask = new DbUpdateTask();
		updateTask.initState();

		assertNotEquals(updateTask.getLastChargesParkingWebTimestamp(), updateTask.getLastChargesParkingWebViewerTimestamp());

		// When
		updateTask.doUpdate();

		// Then
		assertEquals(updateTask.getLastChargesParkingWebTimestamp(), updateTask.getLastChargesParkingWebViewerTimestamp());
	}
}