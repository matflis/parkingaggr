package pl.matflis.parkingweb.core.subscriptions.model;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ViolationType {
	REMINDER("Przypomnienie", 0),
	LACK_OF_TICKET("Brak biletu", 50),
	ILLICIT_PARKING("Parkowanie w niedozwolonym miejscu", 100),
	ILLICIT_VEHICLE_IN_ZONE("Pojazd zabroniony w strefie", 150);

	private final String description;
	private final int amount;

	ViolationType(String description, int amount) {
		this.description = description;
		this.amount = amount;
	}

	@JsonValue
	public String getDescription() {
		return description;
	}

	public int getAmount() {
		return amount;
	}

	public String getAmountAsString() {
		return String.valueOf(getAmount());
	}

	public static ViolationType fromDescription(String description) {
		for (ViolationType type : ViolationType.values()) {
			if (type.getDescription().equalsIgnoreCase(description)) {
				return type;
			}
		}
		throw new IllegalArgumentException("Cannot find violation type with description: " + description);
	}
}