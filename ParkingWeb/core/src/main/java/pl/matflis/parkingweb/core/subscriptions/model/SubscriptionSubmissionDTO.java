package pl.matflis.parkingweb.core.subscriptions.model;

import pl.matflis.parkingweb.core.common.Position;

import java.io.Serializable;

public class SubscriptionSubmissionDTO implements Serializable {

	private final String userName;
	private final SubscriptionEntryInfo subscriptionEntryInfo;
	private final Position position;

	@SuppressWarnings("unused" /* Required for Jackson purposes */)
	private SubscriptionSubmissionDTO() {
		this.userName = null;
		this.subscriptionEntryInfo = null;
		this.position = null;
	}

	public SubscriptionSubmissionDTO(String userName, SubscriptionEntryInfo subscriptionEntryInfo, Position position) {
		this.userName = userName;
		this.subscriptionEntryInfo = subscriptionEntryInfo;
		this.position = position;
	}

	public String getUserName() {
		return userName;
	}

	public SubscriptionEntryInfo getSubscriptionEntryInfo() {
		return subscriptionEntryInfo;
	}

	public Position getPosition() {
		return position;
	}
}