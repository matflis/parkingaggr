package pl.matflis.parkingweb.core.subscriptions.model;

public class CheckSubscriptionDto {
	private final String streetName;
	private final String registrationNumber;

	@SuppressWarnings("unused" /* Jackson only */)
	private CheckSubscriptionDto() {
		this.streetName = null;
		this.registrationNumber = null;
	}

	public CheckSubscriptionDto(String registrationNumber, String streetName) {
		this.registrationNumber = registrationNumber;
		this.streetName = streetName;
	}

	public String getStreetName() {
		return streetName;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}
}