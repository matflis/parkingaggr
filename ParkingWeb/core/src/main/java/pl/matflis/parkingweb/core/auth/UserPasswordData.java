package pl.matflis.parkingweb.core.auth;

import java.io.Serializable;

public final class UserPasswordData implements Serializable {

	private final String userName;
	private final String password;

	@SuppressWarnings("unused" /* Jackson */)
	private UserPasswordData() {
		this.userName = null;
		this.password = null;
	}

	public UserPasswordData(String userName, String password) {
		this.userName = userName;
		this.password = password;
	}

	public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return password;
	}
}