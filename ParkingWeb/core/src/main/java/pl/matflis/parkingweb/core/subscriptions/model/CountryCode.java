package pl.matflis.parkingweb.core.subscriptions.model;

import static java.lang.String.format;

public enum CountryCode {
    PL, FR, GR, SP, PO;

    public static CountryCode fromString(String value) {
        for (CountryCode code : CountryCode.values()) {
            if (code.toString().equalsIgnoreCase(value)) {
                return code;
            }
        }
        throw new IllegalArgumentException(format("Cannot extract country code from %s", value));
    }

    public static CountryCode getDefault() {
        return PL;
    }
}