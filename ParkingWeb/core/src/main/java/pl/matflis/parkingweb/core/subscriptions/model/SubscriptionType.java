package pl.matflis.parkingweb.core.subscriptions.model;

public enum SubscriptionType {
	SUBSCRIPTION_NOT_NEEDED(false), NO_SUBSCRIPTION(false), TICKET(true), SUBSCRIPTION(true);

	private final boolean hasValidityDate;

	SubscriptionType(boolean hasValidityDate) {
		this.hasValidityDate = hasValidityDate;
	}

	public boolean hasValidityDate() {
		return hasValidityDate;
	}
}