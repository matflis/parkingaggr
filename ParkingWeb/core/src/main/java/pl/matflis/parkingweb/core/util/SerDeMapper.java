package pl.matflis.parkingweb.core.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;

import java.io.IOException;

public final class SerDeMapper {

	public static <T> T fromJson(String value, Class<T> destinationClass) throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.registerModule(new JodaModule());
		return objectMapper.readValue(value, destinationClass);
	}

	public static <T> String toJson(T value) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.registerModule(new JodaModule());
		return objectMapper.writeValueAsString(value);
	}

	private SerDeMapper() {
		throw new IllegalStateException("Instantiating SerDeMapper is not allowed");
	}
}