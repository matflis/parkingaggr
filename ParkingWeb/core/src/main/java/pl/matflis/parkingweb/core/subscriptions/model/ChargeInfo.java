package pl.matflis.parkingweb.core.subscriptions.model;

import org.joda.time.DateTime;
import pl.matflis.parkingweb.core.common.Position;

import java.io.Serializable;

public class ChargeInfo implements Serializable {

	private final SubscriptionDTO subscriptionDTO;
	private final Position position;
	private final DateTime offenceTime;
	private final String username;

	@SuppressWarnings("unused" /* Jackson */)
	private ChargeInfo() {
		this.subscriptionDTO = null;
		this.position = null;
		this.offenceTime = null;
		this.username = null;
	}

	public ChargeInfo(SubscriptionDTO subscriptionDTO, Position position, DateTime offenceTime, String username) {
		this.subscriptionDTO = subscriptionDTO;
		this.position = position;
		this.offenceTime = offenceTime;
		this.username = username;
	}

	public SubscriptionDTO getSubscriptionDTO() {
		return subscriptionDTO;
	}

	public DateTime getOffenceTime() {
		return offenceTime;
	}

	public String getUsername() {
		return username;
	}

	public Position getPosition() {
		return position;
	}
}