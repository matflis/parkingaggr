package pl.matflis.parkingweb.core.subscriptions.model;

import java.io.Serializable;

public class ChargeResult implements Serializable {

	private final ViolationType violationType;
	private final long chargeAmount;

	@SuppressWarnings("unused" /* Jackson requirements */)
	private ChargeResult() {
		this.violationType = null;
		this.chargeAmount = 0;
	}

	public ChargeResult(ViolationType violationType, long chargeAmount) {
		this.violationType = violationType;
		this.chargeAmount = chargeAmount;
	}

	public ViolationType getViolationType() {
		return violationType;
	}

	public long getChargeAmount() {
		return chargeAmount;
	}
}