package pl.matflis.parkingweb.core.subscriptions.model;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.io.Serializable;

public final class SubscriptionEntryInfo implements Serializable {

	private final SubscriptionDTO subscriptionDTO;
	private final String filePath;
	private final DateTime offenceTime;

	@SuppressWarnings("unused" /* Required for Jackson purposes */)
	private SubscriptionEntryInfo() {
		this.subscriptionDTO = null;
		this.filePath = null;
		this.offenceTime = null;
	}

	public SubscriptionEntryInfo(SubscriptionDTO subscriptionDTO) {
		this(subscriptionDTO, null);
	}

	public SubscriptionEntryInfo(SubscriptionDTO subscriptionDTO, String filePath) {
		this(subscriptionDTO, filePath, DateTime.now(DateTimeZone.UTC));
	}

	public SubscriptionEntryInfo(SubscriptionDTO subscriptionDTO, String filePath, DateTime offenceTime) {
		this.subscriptionDTO = subscriptionDTO;
		this.filePath = filePath;
		this.offenceTime = offenceTime;
	}

	public SubscriptionDTO getSubscriptionDTO() {
		return subscriptionDTO;
	}

	public String getFilePath() {
		return filePath;
	}

	public DateTime getOffenceTime() {
		return offenceTime;
	}
}