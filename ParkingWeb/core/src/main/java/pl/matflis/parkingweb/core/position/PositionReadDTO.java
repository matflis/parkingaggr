package pl.matflis.parkingweb.core.position;

import pl.matflis.parkingweb.core.common.Position;

import java.io.Serializable;

public class PositionReadDTO implements Serializable {

	private final Position position;
	private final String username;

	private PositionReadDTO() {
		this.position = null;
		this.username = null;
	}

	public PositionReadDTO(Position position, String username) {
		this.position = position;
		this.username = username;
	}

	public Position getPosition() {
		return position;
	}

	public String getUsername() {
		return username;
	}
}