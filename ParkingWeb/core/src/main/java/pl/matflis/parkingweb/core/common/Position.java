package pl.matflis.parkingweb.core.common;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Position implements Serializable {

	private final double longitude;
	private final double latitude;
	private final long readTime;

	@SuppressWarnings("unused" /* Required for Jackson purposes */)
	private Position() {
		this.longitude = 0;
		this.latitude = 0;
		this.readTime = 0;
	}

	public Position(double longitude, double latitude) {
		this.longitude = longitude;
		this.latitude = latitude;
		this.readTime = System.currentTimeMillis();
	}

	public double getLongitude() {
		return longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public long getReadTime() {
		return readTime;
	}

	@Override
	public String toString() {
		Date date = new Date(readTime);
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS", Locale.getDefault());
		return String.format("Longitude: %s, latitude: %s at %s", longitude, latitude, formatter.format(date));
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Position position = (Position) o;

		if (Double.compare(position.longitude, longitude) != 0) {
			return false;
		}
		return Double.compare(position.latitude, latitude) == 0;
	}

	@Override
	public int hashCode() {
		int result;
		long temp;
		temp = Double.doubleToLongBits(longitude);
		result = (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(latitude);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
}