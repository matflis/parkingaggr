package pl.matflis.parkingweb.core.subscriptions.model;

import org.joda.time.DateTime;

import java.io.Serializable;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;


public class SubscriptionInfo implements Serializable {

	private static final String TIME_FORMAT = "yyyy-MM-dd HH:mm";
	private final SubscriptionType subscriptionType;
	private final DateTime validTill;

	@SuppressWarnings("unused" /* Jackson only */)
	private SubscriptionInfo() {
		this.subscriptionType = null;
		this.validTill = null;
	}

	public SubscriptionInfo(SubscriptionType subscriptionType, DateTime validTill) {
		this.subscriptionType = checkNotNull(subscriptionType, "Subscription info has to be specified");
		this.validTill = checkNotNull(validTill, "Validity date has to be specified");
	}

	public SubscriptionInfo(SubscriptionType subscriptionType) {
		checkArgument(!subscriptionType.hasValidityDate());
		this.subscriptionType = subscriptionType;
		this.validTill = null;
	}

	public SubscriptionType getSubscriptionType() {
		return subscriptionType;
	}

	@SuppressWarnings("unused" /* Jackson */)
	public DateTime getValidTill() {
		return validTill;
	}

	@Override
	public String toString() {
		switch (subscriptionType) {
			case SUBSCRIPTION_NOT_NEEDED:
				return "Abonament nie jest wymagany";
			case NO_SUBSCRIPTION:
				return "To auto nie posiada zadnego abonamentu";
			case TICKET:
				return "To auto posiada bilet wazny do: " + validTill.toString(TIME_FORMAT);
			case SUBSCRIPTION:
				return "To auto posiada abonament wazny do: " + validTill.toString(TIME_FORMAT);
			default:
				throw new IllegalStateException("Subscription type: " + this.subscriptionType + " is not handled");
		}
	}
}