package pl.matflis.parkingweb.core.subscriptions.model;


import java.io.Serializable;

public class RegistrationInfo implements Serializable {

    private final String registrationNumber;
    private final CountryCode countryCode;
    private final RegistrationType registrationType;

    public RegistrationInfo(String registrationNumber, CountryCode countryCode, RegistrationType registrationType) {
        this.registrationNumber = registrationNumber;
        this.countryCode = countryCode;
        this.registrationType = registrationType;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public CountryCode getCountryCode() {
        return countryCode;
    }

    public RegistrationType getRegistrationType() {
        return registrationType;
    }

    public enum RegistrationType {
        NEW, OLD;

        public static RegistrationType getDefault() {
            return NEW;
        }
    }
}