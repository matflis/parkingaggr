package pl.matflis.parkingweb.core.subscriptions.model;

import pl.matflis.parkingweb.core.subscriptions.model.RegistrationInfo.RegistrationType;

import java.io.Serializable;
import java.util.UUID;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

public class SubscriptionDTO implements Serializable {

	private final String uuid;
	private final String streetName;
	private final String registrationNumber;
	private final CountryCode countryCode;
	private final RegistrationType registrationType;
	private final String brand;
	private final String remarks;
	private final ViolationType violationType;
	private final int ticketAmount;

	@SuppressWarnings("unused" /* Required for Jackson purposes */)
	private SubscriptionDTO() {
		this.uuid = null;
		this.streetName = null;
		this.registrationNumber = null;
		this.countryCode = null;
		this.registrationType = null;
		this.brand = null;
		this.remarks = null;
		this.violationType = null;
		this.ticketAmount = 0;
	}

	private SubscriptionDTO(SubscriptionDTOBuilder builder) {
		checkArgument(builder.ticketAmount > 0, "Ticket fee must be positive");

		this.uuid = UUID.randomUUID().toString();
		this.streetName = checkNotNull(builder.streetName, "Street name has to be specified");
		this.registrationNumber = checkNotNull(builder.registrationNumber, "Registration number has to be specified");
		this.countryCode = builder.countryCode;
		this.registrationType = builder.registrationType;
		this.brand = checkNotNull(builder.brand, "Brand has to be specified");
		this.remarks = builder.remarks;
		this.violationType = checkNotNull(builder.violationType, "Violation type is required");
		this.ticketAmount = builder.ticketAmount;
	}

	public String getUuid() {
		return uuid;
	}

	public String getStreetName() {
		return streetName;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public CountryCode getCountryCode() {
		return countryCode;
	}

	public RegistrationType getRegistrationType() {
		return registrationType;
	}

	public String getBrand() {
		return brand;
	}

	public String getRemarks() {
		return remarks;
	}

	public ViolationType getViolationType() {
		return violationType;
	}

	public int getTicketAmount() {
		return ticketAmount;
	}

	public static class SubscriptionDTOBuilder {

		private String streetName;
		private String registrationNumber;
		private CountryCode countryCode = CountryCode.getDefault();
		private RegistrationType registrationType = RegistrationType.getDefault();
		private String brand;
		private String remarks;
		private ViolationType violationType;
		private int ticketAmount;

		public SubscriptionDTOBuilder withStreetName(String streetName) {
			this.streetName = streetName;
			return this;
		}

		public SubscriptionDTOBuilder withRegistrationNumber(String registrationNumber) {
			this.registrationNumber = registrationNumber;
			return this;
		}

		public SubscriptionDTOBuilder withCountryCode(CountryCode countryCode) {
			this.countryCode = countryCode;
			return this;
		}

		public SubscriptionDTOBuilder withRegistrationType(RegistrationType registrationType) {
			this.registrationType = registrationType;
			return this;
		}

		public SubscriptionDTOBuilder withBrand(String brand) {
			this.brand = brand;
			return this;
		}

		public SubscriptionDTOBuilder withRemarks(String remarks) {
			this.remarks = remarks;
			return this;
		}

		public SubscriptionDTOBuilder withViolationType(ViolationType violationType) {
			this.violationType = violationType;
			return this;
		}

		public SubscriptionDTOBuilder withTicketAmount(int ticketAmount) {
			this.ticketAmount = ticketAmount;
			return this;
		}

		public SubscriptionDTO build() {
			return new SubscriptionDTO(this);
		}
	}
}