package pl.matflis.parkingweb.server.server;

import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.matflis.parkingweb.server.exception.ParkingWebException;
import pl.matflis.parkingweb.server.handler.LoginHandler;
import pl.matflis.parkingweb.server.handler.PositionUpdateHandler;
import pl.matflis.parkingweb.server.handler.subscriptions.ChargeHandler;
import pl.matflis.parkingweb.server.handler.subscriptions.CheckSubscriptionHandler;
import pl.matflis.parkingweb.server.handler.subscriptions.RecognisePlateHandler;
import ratpack.error.ServerErrorHandler;
import ratpack.guice.Guice;
import ratpack.handling.Context;
import ratpack.server.RatpackServer;

import javax.net.ssl.KeyManagerFactory;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

public class ServerMain {

	private static final Logger logger = LoggerFactory.getLogger(ServerMain.class);
	private static final String KEYSTORE_PASSWORD = "changeit";

	public static void main(String[] args) throws Exception {
		final KeyManagerFactory keyManagerFactory = getKeyManagerFactory();
		final SslContext sslContext = SslContextBuilder
				.forServer(keyManagerFactory)
				.build();
		RatpackServer.start(server -> server
				.serverConfig(serverConfigBuilder ->
						serverConfigBuilder.development(false)
								.ssl(sslContext)
								.build())
				.registry(Guice.registry(bindingsSpec -> bindingsSpec
						.bind(ServerErrorHandler.class, CustomErrorHandler.class)
				))
				.handlers(chain -> chain
						.post(Endpoints.LOGIN, new LoginHandler())
						.post(Endpoints.POSITIONS, new PositionUpdateHandler())
						.prefix(Endpoints.SUBSCRIPTIONS_BASE, subscriptions ->
								subscriptions.post(Endpoints.CHARGE, new ChargeHandler())
										.post(Endpoints.CHECK, new CheckSubscriptionHandler())
										.post(Endpoints.RECOGNISE, new RecognisePlateHandler())
						) //
				) //
		);
	}

	private static KeyManagerFactory getKeyManagerFactory() throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException, UnrecoverableKeyException {
		final KeyStore keyStore = KeyStore.getInstance("JKS");
		try (InputStream is = getKeyStoreStream()) {
			keyStore.load(is, KEYSTORE_PASSWORD.toCharArray());
		}
		final KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		kmf.init(keyStore, KEYSTORE_PASSWORD.toCharArray());
		return kmf;
	}

	private static InputStream getKeyStoreStream() {
		return ServerMain.class.getClassLoader().getResourceAsStream("security/keystore.jks");
	}

	private static class CustomErrorHandler implements ServerErrorHandler {

		@Override
		public void error(Context context, Throwable throwable) throws Exception {
			int status = throwable instanceof ParkingWebException ?
					((ParkingWebException) throwable).getHttpStatus() : HttpURLConnection.HTTP_INTERNAL_ERROR;
			logger.error(throwable.getMessage(), throwable);
			context.getResponse().status(status).send(throwable.getMessage());
		}
	}
}