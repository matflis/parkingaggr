package pl.matflis.parkingweb.server.db;

import org.sql2o.Connection;
import pl.matflis.parkingweb.core.common.Position;

import static com.google.common.base.Preconditions.checkNotNull;

public final class PositionsDao extends BaseDao {

	private static final PositionsDao Instance = new PositionsDao();

	public static PositionsDao getInstance() {
		return Instance;
	}

	private PositionsDao() {
	}

	public void insert(Position readPosition, Long userId) {
		checkNotNull(readPosition, "Position has to be specified");

		final String query = "INSERT INTO Positions VALUES (:longitude, :latitude, :readTime, :userId)";
		try (Connection connection = sql2o.beginTransaction()) {
			connection.createQuery(query)
					.addParameter("longitude", readPosition.getLongitude())
					.addParameter("latitude", readPosition.getLatitude())
					.addParameter("readTime", readPosition.getReadTime())
					.addParameter("userId", userId)
					.executeUpdate();
			connection.commit();
		}
	}
}