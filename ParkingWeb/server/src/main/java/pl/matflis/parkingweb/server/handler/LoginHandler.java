package pl.matflis.parkingweb.server.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.matflis.parkingweb.core.auth.UserPasswordData;
import pl.matflis.parkingweb.core.util.SerDeMapper;
import pl.matflis.parkingweb.server.exception.UserNotFoundException;
import pl.matflis.parkingweb.server.service.AuthorizationService;
import ratpack.exec.Promise;
import ratpack.handling.Context;
import ratpack.handling.Handler;
import ratpack.http.TypedData;

public class LoginHandler implements Handler {

	private static final Logger logger = LoggerFactory.getLogger(LoginHandler.class);

	private final AuthorizationService authorizationService = AuthorizationService.getInstance();

	@Override
	public void handle(Context ctx) throws Exception {
		logger.debug("Handling login request");
		final Promise<TypedData> content = ctx.getRequest().getBody();
		content.then(data -> {
			final UserPasswordData userPasswordData = SerDeMapper.fromJson(data.getText(), UserPasswordData.class);
			final String userName = userPasswordData.getUserName();
			logger.debug("User {} is trying to log in", userName);

			final boolean result = authorizationService.authorize(userPasswordData);
			if (result) {
				logger.info("User {} logged in successfully", userName);
				ctx.render("Login successful");
			} else {
				logger.warn("User {} has not been logged in", userName);
				throw new UserNotFoundException("Cannot log in. Wrong username or password");
			}
		});
	}
}