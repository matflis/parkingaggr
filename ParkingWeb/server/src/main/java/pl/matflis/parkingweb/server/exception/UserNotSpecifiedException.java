package pl.matflis.parkingweb.server.exception;

public class UserNotSpecifiedException extends BadRequestException {

	public UserNotSpecifiedException(String message) {
		super(message);
	}
}