package pl.matflis.parkingweb.server.exception;

public class UserNotFoundException extends NotFoundException {

	public UserNotFoundException(String message) {
		super(message);
	}
}