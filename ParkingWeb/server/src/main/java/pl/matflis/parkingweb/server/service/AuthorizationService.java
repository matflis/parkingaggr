package pl.matflis.parkingweb.server.service;

import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;
import pl.matflis.parkingweb.core.auth.UserPasswordData;
import pl.matflis.parkingweb.server.db.UsersDao;

import java.nio.charset.Charset;

public final class AuthorizationService {

	private static final AuthorizationService Instance = new AuthorizationService();

	private final UsersDao usersDao = UsersDao.getInstance();

	public static AuthorizationService getInstance() {
		return Instance;
	}

	public boolean authorize(UserPasswordData userPasswordData) {
		final HashCode hashedPassword = Hashing.sha1().hashString(userPasswordData.getPassword(), Charset.forName("UTF-8"));
		final String hashedPasswordString = hashedPassword.toString();
		return usersDao.authorize(userPasswordData.getUserName(), hashedPasswordString);
	}

	private AuthorizationService() {
	}
}