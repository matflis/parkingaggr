package pl.matflis.parkingweb.server.exception;

import java.net.HttpURLConnection;

public class NotInZoneException extends ParkingWebException {
	public NotInZoneException() {
		super("Vehicle not in ticket zone", HttpURLConnection.HTTP_BAD_REQUEST);
	}
}