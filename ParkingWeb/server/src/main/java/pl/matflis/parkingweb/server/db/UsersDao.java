package pl.matflis.parkingweb.server.db;

import org.apache.commons.lang3.StringUtils;
import org.sql2o.Connection;

import java.util.Optional;

import static com.google.common.base.Preconditions.checkArgument;

public final class UsersDao extends BaseDao {

	private static final UsersDao Instance = new UsersDao();

	private UsersDao() {
	}

	public static UsersDao getInstance() {
		return Instance;
	}

	public Optional<Long> getUserId(String userName) {
		checkArgument(StringUtils.isNotBlank(userName), "Username cannot be blank");

		final String query = "SELECT ID FROM Users WHERE LOWER(Username) = :userName";
		try (Connection connection = sql2o.open()) {
			final Long userId = connection.createQuery(query)
					.addParameter("userName", userName.toLowerCase())
					.executeAndFetchFirst(Long.class);
			return Optional.ofNullable(userId);
		}
	}

	public boolean authorize(String userName, String password) {
		checkArgument(StringUtils.isNotBlank(userName), "Username has to be specified");
		checkArgument(StringUtils.isNotBlank(password), "Password has to be specified");

		final String query = "SELECT COUNT(*) FROM Users WHERE LOWER(Username) = :userName " +
				"AND Password = :password";
		try (Connection connection = sql2o.open()) {
			return connection.createQuery(query)
					.addParameter("userName", userName.toLowerCase())
					.addParameter("password", password)
					.executeAndFetchFirst(Long.class) > 0;
		}
	}
}