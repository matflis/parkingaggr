package pl.matflis.parkingweb.server.service;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.matflis.parkingweb.core.subscriptions.model.ChargeResult;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionDTO;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionEntryInfo;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionInfo;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionSubmissionDTO;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionType;
import pl.matflis.parkingweb.core.subscriptions.model.ViolationType;
import pl.matflis.parkingweb.server.db.ChargeDao;
import pl.matflis.parkingweb.server.db.SubscriptionDao;
import pl.matflis.parkingweb.server.db.UsersDao;
import pl.matflis.parkingweb.server.db.ZoneDao;
import pl.matflis.parkingweb.server.exception.NotInZoneException;
import pl.matflis.parkingweb.server.exception.SubscriptionAlreadyExistsException;
import pl.matflis.parkingweb.server.exception.UserNotFoundException;
import pl.matflis.parkingweb.server.exception.UserNotSpecifiedException;
import pl.matflis.parkingweb.server.model.ChargeApplyInfo;

import java.util.Optional;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static pl.matflis.parkingweb.server.model.ChargeApplyInfo.ChargeApplyInfoBuilder;

public final class SubscriptionService {

	private static final Logger logger = LoggerFactory.getLogger(SubscriptionService.class);

	private static final SubscriptionService Instance = new SubscriptionService();

	private final SubscriptionDao subscriptionDao = SubscriptionDao.getInstance();
	private final ZoneDao zoneDao = ZoneDao.getInstance();
	private final ChargeDao chargeDao = ChargeDao.getInstance();
	private final UsersDao usersDao = UsersDao.getInstance();

	public static SubscriptionService getInstance() {
		return Instance;
	}

	private SubscriptionService() {
	}

	public ChargeResult charge(SubscriptionSubmissionDTO submission) {
		checkNotNull(submission, "Submission has to be specified");
		// Check if user has a ticket
		final SubscriptionEntryInfo subscriptionEntryInfo = checkNotNull(submission.getSubscriptionEntryInfo(),
				"SubmissionEntry must exist");
		final SubscriptionDTO subscriptionDTO = checkNotNull(subscriptionEntryInfo.getSubscriptionDTO(),
				"SubscriptionDto must be specified");
		final String userName = submission.getUserName();
		if (StringUtils.isBlank(userName)) {
			throw new UserNotSpecifiedException("Username was not specified");
		}
		Optional<Long> zoneNumber = zoneDao.getZoneNumber(subscriptionDTO.getStreetName());
		if (!zoneNumber.isPresent()) {
			logger.warn("Trying to insert ticket for a vehicle that is not placed in ticket zone. Controller name: {}, " +
					"registration plate: {}", userName, subscriptionDTO.getRegistrationNumber());
			throw new NotInZoneException();
		}
		if (subscriptionDao.countWithPlate(subscriptionDTO.getRegistrationNumber(), zoneNumber.get()) > 0) {
			logger.warn("Trying to give a ticket to person that already has subscription. Controller name: {}," +
					" registration plate: {} ", userName, subscriptionDTO.getRegistrationNumber());
			throw new SubscriptionAlreadyExistsException("Cannot give a ticket. Registration plate has subscription bought");
		}
		final long userId = usersDao.getUserId(userName)
				.orElseThrow(() -> new UserNotFoundException("User with name '" + userName + "' does not exist"));

		Optional<Long> expirationOptional = subscriptionDao.getExpirationTime(subscriptionDTO.getRegistrationNumber(), zoneNumber.get());
		return chargeVehicle(submission, subscriptionDTO, userId, expirationOptional);
	}

	private ChargeResult chargeVehicle(SubscriptionSubmissionDTO submission, SubscriptionDTO subscriptionDTO, long userId, Optional<Long> expirationOptional) {
		return expirationOptional.map(expirationTime -> {
			ViolationSpecifics violationSpecifics = getViolationSpecifics(expirationTime, subscriptionDTO);

			ChargeApplyInfo chargeInfo = new ChargeApplyInfoBuilder()
					.withUserId(userId)
					.withLongitude(submission.getPosition().getLongitude())
					.withLatitude(submission.getPosition().getLatitude())
					.withOffenceTimestamp(submission.getSubscriptionEntryInfo().getOffenceTime().getMillis())
					.withStreetName(subscriptionDTO.getStreetName())
					.withRegistrationPlate(subscriptionDTO.getRegistrationNumber())
					.withCountryCode(subscriptionDTO.getCountryCode().name())
					.withBrand(subscriptionDTO.getBrand())
					.withRemarks(subscriptionDTO.getRemarks())
					.withViolationType(violationSpecifics.violationType)
					.withTicketAmount(violationSpecifics.amount)
					.build();
			return chargeVehicle(chargeInfo);
		}).orElseGet(() -> {
			ChargeApplyInfo chargeInfo = new ChargeApplyInfoBuilder()
					.withUserId(userId)
					.withLongitude(submission.getPosition().getLongitude())
					.withLatitude(submission.getPosition().getLatitude())
					.withOffenceTimestamp(submission.getSubscriptionEntryInfo().getOffenceTime().getMillis())
					.withStreetName(subscriptionDTO.getStreetName())
					.withRegistrationPlate(subscriptionDTO.getRegistrationNumber())
					.withCountryCode(subscriptionDTO.getCountryCode().name())
					.withBrand(subscriptionDTO.getBrand())
					.withRemarks(subscriptionDTO.getRemarks())
					.withViolationType(subscriptionDTO.getViolationType().getDescription())
					.withTicketAmount((long) subscriptionDTO.getTicketAmount())
					.build();
			return chargeVehicle(chargeInfo);
		});
	}

	private ViolationSpecifics getViolationSpecifics(Long expirationTime, SubscriptionDTO subscriptionDTO) {
		if (SubscriptionServiceUtils.expiredToday(expirationTime)) {
			return new ViolationSpecifics(ViolationType.REMINDER.getDescription(), SubscriptionServiceUtils.getFine(expirationTime));
		}
		return new ViolationSpecifics(subscriptionDTO.getViolationType().getDescription(), (long) subscriptionDTO.getTicketAmount());
	}

	private ChargeResult chargeVehicle(ChargeApplyInfo chargeInfo) {
		// charge vehicle
		chargeDao.charge(chargeInfo);
		logger.debug("Ticket of type {} to {} given successfully!", chargeInfo.getViolationType(), chargeInfo.getRegistrationPlate());
		return new ChargeResult(ViolationType.fromDescription(chargeInfo.getViolationType()), chargeInfo.getTicketAmount());
	}


	public SubscriptionInfo getSubscriptionInfo(String streetName, String registrationNumber) {
		checkArgument(StringUtils.isNotBlank(registrationNumber), "Registration plate has to be specified");
		checkArgument(StringUtils.isNotBlank(streetName), "StreetName has to be specified");

		return zoneDao.getZoneNumber(streetName)
				.map(zoneNumber -> subscriptionDao.getSubscriptionType(registrationNumber))
				.orElseGet(() -> new SubscriptionInfo(SubscriptionType.SUBSCRIPTION_NOT_NEEDED));
	}

	private class ViolationSpecifics {
		private final String violationType;
		private final Long amount;

		private ViolationSpecifics(String violationType, Long amount) {
			this.violationType = violationType;
			this.amount = amount;
		}
	}
}