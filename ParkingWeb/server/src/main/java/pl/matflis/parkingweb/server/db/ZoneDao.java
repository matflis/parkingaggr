package pl.matflis.parkingweb.server.db;

import org.sql2o.Connection;

import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;

public final class ZoneDao extends BaseDao {

	private static final ZoneDao Instance = new ZoneDao();

	private ZoneDao() {
	}

	public static ZoneDao getInstance() {
		return Instance;
	}

	public Optional<Long> getZoneNumber(String streetName) {
		checkNotNull(streetName, "StreetName has to be specified");
		final String query = "SELECT ZoneId FROM ZoneStreet where LOWER(streetName) = :street";
		try (Connection connection = sql2o.open()) {
			return Optional.ofNullable(connection.createQuery(query)
					.addParameter("street", streetName.toLowerCase())
					.executeAndFetchFirst(Long.class));
		}
	}
}