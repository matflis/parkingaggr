package pl.matflis.parkingweb.server.exception;

import java.net.HttpURLConnection;

abstract class BadRequestException extends ParkingWebException {
	BadRequestException(String message) {
		super(message, HttpURLConnection.HTTP_BAD_REQUEST);
	}
}