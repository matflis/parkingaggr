package pl.matflis.parkingweb.server.exception;

public abstract class ParkingWebException extends RuntimeException {

	private final int httpStatus;

	ParkingWebException(String message, int httpStatus) {
		super(message);
		this.httpStatus = httpStatus;
	}

	public int getHttpStatus() {
		return httpStatus;
	}
}