package pl.matflis.parkingweb.server.exception;

public class PlateRemoteExtractionException extends RuntimeException {

	public PlateRemoteExtractionException(String message) {
		super(message);
	}
}