package pl.matflis.parkingweb.server.server;

final class Endpoints {

	static final String LOGIN = "login";
	static final String POSITIONS = "positions";
	static final String SUBSCRIPTIONS_BASE = "subscriptions";
	static final String CHARGE = "charge";
	static final String CHECK = "check";
	static final String RECOGNISE = "recognise";
	static final String CHARGES = "charges";

	private Endpoints() {
	}
}