package pl.matflis.parkingweb.server.db;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.sql2o.Connection;
import org.sql2o.ResultSetHandler;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionInfo;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionType;

import java.util.Optional;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

public final class SubscriptionDao extends BaseDao {

	private static final SubscriptionDao Instance = new SubscriptionDao();

	private SubscriptionDao() {
	}

	public static SubscriptionDao getInstance() {
		return Instance;
	}

	public long countWithPlate(String plate, Long zoneId) {
		checkArgument(StringUtils.isNotBlank(plate), "Registration plate has to be specified");
		final String query = "SELECT COUNT(*) FROM SUBSCRIPTIONS WHERE LOWER(RegistrationPlate) = :plate AND " +
				"ValidTill >= :validTill AND ZoneId = :zoneId";
		try (Connection connection = sql2o.open()) {
			return connection.createQuery(query)
					.addParameter("plate", plate.toLowerCase())
					.addParameter("validTill", DateTime.now().getMillis())
					.addParameter("zoneId", zoneId)
					.executeAndFetchFirst(Long.class);
		}
	}

	public Optional<Long> getExpirationTime(String plate, Long zoneId) {
		checkArgument(StringUtils.isNotBlank(plate), "Registration plate has to be specified");
		checkNotNull(zoneId, "ZoneId has to be specified");
		final String query = "SELECT ValidTill FROM SUBSCRIPTIONS WHERE LOWER(RegistrationPlate) = :plate AND ZoneId = :zoneId";
		try (Connection connection = sql2o.open()) {
			return Optional.ofNullable(connection.createQuery(query)
					.addParameter("plate", plate.toLowerCase())
					.addParameter("zoneId", zoneId)
					.executeAndFetchFirst(Long.class));
		}
	}

	public SubscriptionInfo getSubscriptionType(String plate) {
		checkArgument(StringUtils.isNotBlank(plate), "Registration plate has to be specified");
		final String query = "SELECT SubscriptionType, ValidTill FROM SUBSCRIPTIONS WHERE LOWER(RegistrationPlate) " +
				"= :plate AND ValidTill >= :validTill";
		try (Connection connection = sql2o.open()) {
			final Optional<SubscriptionInfo> subscriptionInfo = Optional.ofNullable(connection.createQuery(query)
					.addParameter("plate", plate.toLowerCase())
					.addParameter("validTill", DateTime.now().getMillis())
					.executeAndFetchFirst((ResultSetHandler<SubscriptionInfo>) resultSet -> {
						SubscriptionType subscriptionType = SubscriptionType.valueOf(resultSet.getString("SubscriptionType"));
						DateTime validTill = new DateTime(resultSet.getLong("ValidTill"), DateTimeZone.UTC);
						return new SubscriptionInfo(subscriptionType, validTill);
					}));
			return subscriptionInfo
					.orElseGet(() -> new SubscriptionInfo(SubscriptionType.NO_SUBSCRIPTION));
		}
	}
}