package pl.matflis.parkingweb.server.handler.subscriptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.matflis.parkingweb.core.subscriptions.model.ChargeResult;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionSubmissionDTO;
import pl.matflis.parkingweb.core.util.SerDeMapper;
import pl.matflis.parkingweb.server.service.SubscriptionService;
import ratpack.exec.Promise;
import ratpack.handling.Context;
import ratpack.handling.Handler;
import ratpack.http.TypedData;

public class ChargeHandler implements Handler {

	private static final Logger logger = LoggerFactory.getLogger(ChargeHandler.class);

	private final SubscriptionService subscriptionService = SubscriptionService.getInstance();

	@Override
	public void handle(Context ctx) {
		logger.debug("Handling Charge Handler");
		final Promise<TypedData> content = ctx.getRequest().getBody();
		content.then(data -> {
			SubscriptionSubmissionDTO submission = SerDeMapper.fromJson(data.getText(), SubscriptionSubmissionDTO.class);
			logger.debug("Subscription registered from user: {}", submission.getUserName());

			ChargeResult result = subscriptionService.charge(submission);
			ctx.render(SerDeMapper.toJson(result));
		});
	}
}