package pl.matflis.parkingweb.server.exception;

import java.net.HttpURLConnection;

abstract class NotFoundException extends ParkingWebException {
	NotFoundException(String message) {
		super(message, HttpURLConnection.HTTP_NOT_FOUND);
	}
}