package pl.matflis.parkingweb.server.exception;

import java.net.HttpURLConnection;

public class SubscriptionAlreadyExistsException extends ParkingWebException {
	public SubscriptionAlreadyExistsException(String message) {
		super(message, HttpURLConnection.HTTP_CONFLICT);
	}
}