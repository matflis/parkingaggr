package pl.matflis.parkingweb.server.db;

import org.sql2o.Sql2o;

abstract class BaseDao {

	final Sql2o sql2o;

	BaseDao() {
		this.sql2o = DbProvider.getDbConnector();
	}
}