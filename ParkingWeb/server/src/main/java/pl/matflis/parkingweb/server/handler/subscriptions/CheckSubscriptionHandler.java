package pl.matflis.parkingweb.server.handler.subscriptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.matflis.parkingweb.core.subscriptions.model.CheckSubscriptionDto;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionInfo;
import pl.matflis.parkingweb.core.util.SerDeMapper;
import pl.matflis.parkingweb.server.service.SubscriptionService;
import ratpack.exec.Promise;
import ratpack.handling.Context;
import ratpack.handling.Handler;
import ratpack.http.TypedData;

public class CheckSubscriptionHandler implements Handler {

	private static final Logger logger = LoggerFactory.getLogger(CheckSubscriptionHandler.class);

	private final SubscriptionService subscriptionService = SubscriptionService.getInstance();

	@Override
	public void handle(Context ctx) {
		logger.debug("Handling checking subscription");
		final Promise<TypedData> content = ctx.getRequest().getBody();
		content.then(data -> {
			CheckSubscriptionDto subscription = SerDeMapper.fromJson(data.getText(), CheckSubscriptionDto.class);
			logger.debug("Checking subscription for plate '{}'", subscription.getRegistrationNumber());

			SubscriptionInfo subscriptionInfo = subscriptionService.getSubscriptionInfo(subscription.getStreetName(), subscription.getRegistrationNumber());
			ctx.render(SerDeMapper.toJson(subscriptionInfo));
		});
	}
}