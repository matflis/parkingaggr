package pl.matflis.parkingweb.server.db;

import org.sql2o.Connection;
import pl.matflis.parkingweb.server.model.ChargeApplyInfo;

import static com.google.common.base.Preconditions.checkNotNull;

public final class ChargeDao extends BaseDao {

	private static final ChargeDao Instance = new ChargeDao();

	private ChargeDao() {
	}

	public static ChargeDao getInstance() {
		return Instance;
	}

	public void charge(ChargeApplyInfo chargeApplyInfo) {
		checkNotNull(chargeApplyInfo, "Submission info must be specified");

		final String insertQuery = "INSERT INTO Charges (UserID, Longitude, Latitude, OffenceTimestamp, StreetName," +
				"RegistrationPlate, CountryCode, Brand, Remarks, ViolationType, TicketAmount) VALUES (:userId, " +
				":longitude, :latitude, :offenceTime, :street, :plate, :countryCode, :brand, :remarks, :violation, :amount)";

		try (Connection connection = sql2o.beginTransaction()) {
			connection.createQuery(insertQuery)
					.addParameter("userId", chargeApplyInfo.getUserId())
					.addParameter("longitude", chargeApplyInfo.getLongitude())
					.addParameter("latitude", chargeApplyInfo.getLatitude())
					.addParameter("offenceTime", chargeApplyInfo.getOffenceTimestamp())
					.addParameter("street", chargeApplyInfo.getStreetName())
					.addParameter("plate", chargeApplyInfo.getRegistrationPlate())
					.addParameter("countryCode", chargeApplyInfo.getCountryCode())
					.addParameter("brand", chargeApplyInfo.getBrand())
					.addParameter("remarks", chargeApplyInfo.getRemarks())
					.addParameter("violation", chargeApplyInfo.getViolationType())
					.addParameter("amount", chargeApplyInfo.getTicketAmount())
					.executeUpdate();
			connection.commit();
		}
	}
}