package pl.matflis.parkingweb.server.service;

import pl.matflis.parkingweb.core.position.PositionReadDTO;
import pl.matflis.parkingweb.server.db.PositionsDao;
import pl.matflis.parkingweb.server.db.UsersDao;
import pl.matflis.parkingweb.server.exception.UserNotFoundException;

import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;

public final class PositionsService {

	private static final PositionsService Instance = new PositionsService();

	private final UsersDao usersDao = UsersDao.getInstance();
	private final PositionsDao positionsDao = PositionsDao.getInstance();

	public static PositionsService getInstance() {
		return Instance;
	}

	public void addPosition(PositionReadDTO positionReadDTO) {
		checkNotNull(positionReadDTO, "Position read has to be specified");
		final String username = positionReadDTO.getUsername();
		final Optional<Long> userIdOptional = usersDao.getUserId(username);
		if (userIdOptional.isPresent()) {
			positionsDao.insert(positionReadDTO.getPosition(), userIdOptional.get());
		} else {
			throw new UserNotFoundException("Username: " + username + " not found");
		}
	}

	private PositionsService() {
	}
}