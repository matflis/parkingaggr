package pl.matflis.parkingweb.server.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.matflis.parkingweb.core.position.PositionReadDTO;
import pl.matflis.parkingweb.core.util.SerDeMapper;
import pl.matflis.parkingweb.server.service.PositionsService;
import ratpack.exec.Promise;
import ratpack.handling.Context;
import ratpack.handling.Handler;
import ratpack.http.TypedData;

public class PositionUpdateHandler implements Handler {

	private static final Logger logger = LoggerFactory.getLogger(PositionUpdateHandler.class);

	private final PositionsService positionsService = PositionsService.getInstance();

	@Override
	public void handle(Context ctx) throws Exception {
		logger.info("Handling position update request");
		final Promise<TypedData> content = ctx.getRequest().getBody();
		content.then(data -> {
			PositionReadDTO positionUpdate = SerDeMapper.fromJson(data.getText(), PositionReadDTO.class);
			positionsService.addPosition(positionUpdate);

			ctx.render("Position inserted successfully");
		});
	}
}