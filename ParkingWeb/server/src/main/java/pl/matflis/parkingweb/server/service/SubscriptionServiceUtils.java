package pl.matflis.parkingweb.server.service;

import org.joda.time.DateTime;

final class SubscriptionServiceUtils {

	private static final long HOUR_WAGE = 3;
	private static final long HOUR = 60 * 60 * 1_000;

	static Long getFine(Long time) {
		double difference = (double) (System.currentTimeMillis() - time) / HOUR;
		return (long) Math.ceil(difference) * HOUR_WAGE;
	}

	static boolean expiredToday(Long time) {
		DateTime dateTime = new DateTime(time);
		DateTime currentTime = new DateTime();
		return dateTime.getYear() == currentTime.getYear() && dateTime.getDayOfYear() == currentTime.getDayOfYear();
	}

	private SubscriptionServiceUtils() {
	}
}