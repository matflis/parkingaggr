package pl.matflis.parkingweb.server.service;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.matflis.parkingweb.server.exception.PlateRemoteExtractionException;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public final class PlateRecognitionService {

	private static final Logger logger = LoggerFactory.getLogger(PlateRecognitionService.class);
	private static final PlateRecognitionService Instance = new PlateRecognitionService();

	public static PlateRecognitionService getInstance() {
		return Instance;
	}

	public Optional<String> getPlate(byte[] image) throws Exception {
		byte[] encodedImage = Base64.getEncoder().encode(image);
		String apiJsonResponse = getApiPlateContent(encodedImage);
		return extractPlate(apiJsonResponse);
	}

	private String getUrl() {
		final String secret_key = "sk_c8e7e3cd0e86090260f806fd";
		return "https://api.openalpr.com/v2/recognize_bytes?recognize_vehicle=1&country=eu&secret_key=" + secret_key;
	}

	private String getApiPlateContent(byte[] encodedImage) throws IOException {
		URL url = new URL(getUrl());
		HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
		httpConnection.setRequestMethod("POST");
		httpConnection.setDoOutput(true);
		httpConnection.setFixedLengthStreamingMode(encodedImage.length);

		try (OutputStream os = httpConnection.getOutputStream()) {
			os.write(encodedImage);
		}

		if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
			return IOUtils.toString(httpConnection.getInputStream(), Charset.defaultCharset());
		}
		throw new PlateRemoteExtractionException("Plate API response code is different than 200");
	}

	@SuppressWarnings("unchecked")
	private Optional<String> extractPlate(String jsonResponse) {
		JSONObject jsonObject = new JSONObject(jsonResponse);
		List<Object> results = jsonObject.getJSONArray("results").toList();
		if (results.isEmpty()) {
			logger.debug("Result response for image recognition is empty");
			return Optional.empty();
		} else {
			Map<String, Object> bestResult = (Map<String, Object>) results.get(0);
			String plateContent = (String) bestResult.get("plate");
			logger.debug("Found plate {} with confidence {}%", plateContent, bestResult.get("confidence"));
			return Optional.of(plateContent);
		}
	}

	private PlateRecognitionService() {
	}
}