package pl.matflis.parkingweb.server.handler.subscriptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.matflis.parkingweb.server.service.PlateRecognitionService;
import ratpack.exec.Promise;
import ratpack.handling.Context;
import ratpack.handling.Handler;
import ratpack.http.TypedData;

import java.net.HttpURLConnection;
import java.util.Optional;

public class RecognisePlateHandler implements Handler {

	private static final Logger logger = LoggerFactory.getLogger(RecognisePlateHandler.class);

	private final PlateRecognitionService plateRecognitionService = PlateRecognitionService.getInstance();

	@Override
	public void handle(Context ctx) throws Exception {
		logger.debug("Handling checking subscription");
		final Promise<TypedData> content = ctx.getRequest().getBody();
		content.then(data -> {
			byte[] imageContent = data.getBytes();
			try {
				Optional<String> plate = plateRecognitionService.getPlate(imageContent);

				if (!plate.isPresent()) {
					logger.info("Plate not recognised");
					ctx.getResponse() //
							.status(HttpURLConnection.HTTP_NOT_FOUND) //
							.send("Cannot find plate from picture");
				} else {
					String plateContent = plate.get();
					logger.info("Plate from picture has been recognised. Content: {}", plateContent);
					ctx.render(plateContent);
				}
			} catch (Exception e) {
				logger.error("Exception while extracting plate using API. Reason: " + e.getMessage(), e);
				ctx.getResponse()
						.status(HttpURLConnection.HTTP_INTERNAL_ERROR)
						.send("Internal error when extracting plate");
			}
		});
	}
}