package pl.matflis.parkingweb.server.model;

import static com.google.common.base.Preconditions.checkNotNull;

public class ChargeApplyInfo {
	private final long userId;
	private final double longitude;
	private final double latitude;
	private final long offenceTimestamp;
	private final String streetName;
	private final String registrationPlate;
	private final String countryCode;
	private final String brand;
	private final String remarks;
	private final String violationType;
	private final long ticketAmount;

	private ChargeApplyInfo(ChargeApplyInfoBuilder builder) {
		this.userId = checkNotNull(builder.userId, "UserId has to be specified");
		this.longitude = checkNotNull(builder.longitude, "Longitude has to be specified");
		this.latitude = checkNotNull(builder.latitude, "Latitude has to be specified");
		this.offenceTimestamp = checkNotNull(builder.offenceTimestamp, "OffenceTime has to be specified");
		this.streetName = checkNotNull(builder.streetName, "StreetName has to be specified");
		this.registrationPlate = checkNotNull(builder.registrationPlate, "RegistrationPlate has to be specified");
		this.countryCode = checkNotNull(builder.countryCode, "CountryCode has to be specified");
		this.brand = checkNotNull(builder.brand, "Brand has to be specified");
		this.remarks = builder.remarks;
		this.violationType = checkNotNull(builder.violationType, "ViolationType has to be specified");
		this.ticketAmount = checkNotNull(builder.ticketAmount, "TicketAmount has to be specified");
	}

	public long getUserId() {
		return userId;
	}

	public double getLongitude() {
		return longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public long getOffenceTimestamp() {
		return offenceTimestamp;
	}

	public String getStreetName() {
		return streetName;
	}

	public String getRegistrationPlate() {
		return registrationPlate;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public String getBrand() {
		return brand;
	}

	public String getRemarks() {
		return remarks;
	}

	public String getViolationType() {
		return violationType;
	}

	public long getTicketAmount() {
		return ticketAmount;
	}

	public static class ChargeApplyInfoBuilder {
		private Long userId;
		private Double longitude;
		private Double latitude;
		private Long offenceTimestamp;
		private String streetName;
		private String registrationPlate;
		private String countryCode;
		private String brand;
		private String remarks;
		private String violationType;
		private Long ticketAmount;

		public ChargeApplyInfoBuilder withUserId(Long userId) {
			this.userId = userId;
			return this;
		}

		public ChargeApplyInfoBuilder withLongitude(Double longitude) {
			this.longitude = longitude;
			return this;
		}

		public ChargeApplyInfoBuilder withLatitude(Double latitude) {
			this.latitude = latitude;
			return this;
		}

		public ChargeApplyInfoBuilder withOffenceTimestamp(Long offenceTimestamp) {
			this.offenceTimestamp = offenceTimestamp;
			return this;
		}

		public ChargeApplyInfoBuilder withStreetName(String streetName) {
			this.streetName = streetName;
			return this;
		}

		public ChargeApplyInfoBuilder withRegistrationPlate(String registrationPlate) {
			this.registrationPlate = registrationPlate;
			return this;
		}

		public ChargeApplyInfoBuilder withCountryCode(String countryCode) {
			this.countryCode = countryCode;
			return this;
		}

		public ChargeApplyInfoBuilder withBrand(String brand) {
			this.brand = brand;
			return this;
		}

		public ChargeApplyInfoBuilder withRemarks(String remarks) {
			this.remarks = remarks;
			return this;
		}

		public ChargeApplyInfoBuilder withViolationType(String violationType) {
			this.violationType = violationType;
			return this;
		}

		public ChargeApplyInfoBuilder withTicketAmount(Long ticketAmount) {
			this.ticketAmount = ticketAmount;
			return this;
		}

		public ChargeApplyInfo build() {
			return new ChargeApplyInfo(this);
		}
	}
}