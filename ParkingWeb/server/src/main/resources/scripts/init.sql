CREATE TABLE IF NOT EXISTS Users (
  ID       BIGINT PRIMARY KEY AUTO_INCREMENT,
  Username VARCHAR(100) NOT NULL UNIQUE,
  Password VARCHAR(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS Zones (
  ZoneId   BIGINT PRIMARY KEY AUTO_INCREMENT,
  ZoneName VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS ZoneStreet (
  ZoneId     BIGINT      NOT NULL,
  StreetName VARCHAR(50) NOT NULL,
  FOREIGN KEY (ZoneId) REFERENCES Zones (ZoneId)
);

CREATE TABLE IF NOT EXISTS Subscriptions (
  ID                BIGINT PRIMARY KEY AUTO_INCREMENT,
  RegistrationPlate VARCHAR(10) NOT NULL,
  SubscriptionType  VARCHAR(12) NOT NULL,
  ValidTill         BIGINT      NOT NULL,
  ZoneId            BIGINT      NOT NULL,
  FOREIGN KEY (ZoneId) REFERENCES Zones (ZoneId),
  CHECK SubscriptionType IN ('TICKET', 'SUBSCRIPTION'),
  CHECK ValidTill > 0
);

CREATE TABLE IF NOT EXISTS Charges (
  ID                BIGINT PRIMARY KEY AUTO_INCREMENT,
  UserID            BIGINT       NOT NULL,
  Longitude         DOUBLE       NOT NULL,
  Latitude          DOUBLE       NOT NULL,
  OffenceTimestamp  BIGINT       NOT NULL,
  StreetName        VARCHAR(100) NOT NULL,
  RegistrationPlate VARCHAR(100) NOT NULL,
  CountryCode       VARCHAR(10)  NOT NULL,
  Brand             VARCHAR(100) NOT NULL,
  Remarks           VARCHAR(100),
  ViolationType     VARCHAR(100) NOT NULL,
  TicketAmount      INT          NOT NULL,
  FOREIGN KEY (UserID) REFERENCES Users (ID),
  CHECK TicketAmount > 0,
  CHECK OffenceTimestamp > 0
);

CREATE TABLE IF NOT EXISTS Positions (
  Longitude     DOUBLE NOT NULL,
  Latitude      DOUBLE NOT NULL,
  ReadTimeStamp BIGINT,
  UserId        BIGINT NOT NULL,
  FOREIGN KEY (UserId) REFERENCES Users (ID),
  CHECK ReadTimeStamp > 0
);

MERGE INTO Users
KEY (ID) VALUES (1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997'); -- means: admin:admin
MERGE INTO Users
KEY (ID) VALUES (2, 'tester', 'd033e22ae348aeb5660fc2140aec35850c4da997'); -- means: tester:admin
