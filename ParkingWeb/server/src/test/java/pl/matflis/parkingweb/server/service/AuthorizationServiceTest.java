package pl.matflis.parkingweb.server.service;

import org.junit.BeforeClass;
import org.junit.Test;
import org.powermock.core.classloader.annotations.PrepareForTest;
import pl.matflis.parkingweb.core.auth.UserPasswordData;
import pl.matflis.parkingweb.server.db.UsersDao;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

@PrepareForTest({UsersDao.class})
public class AuthorizationServiceTest extends BaseServiceTest {

	private static UsersDao usersDao = mock(UsersDao.class);

	private final AuthorizationService authorizationService = AuthorizationService.getInstance();

	@BeforeClass
	public static void init() {
		mockStatic(UsersDao.class);
		when(UsersDao.getInstance()).thenReturn(usersDao);
	}

	@Test
	public void shouldAuthorizeUserWithValidCredentials() {
		verifyAuthorization(true);
	}

	@Test
	public void shouldNotAuthorizeUserWithInvalidCredentials() {
		verifyAuthorization(false);
	}

	private void verifyAuthorization(boolean isValid) {
		// Given
		UserPasswordData userPasswordData = new UserPasswordData("user", "test");
		when(usersDao.authorize(anyString(), anyString())).thenReturn(isValid);

		// When
		final boolean result = authorizationService.authorize(userPasswordData);

		// Then
		assertEquals(isValid, result);
		verify(usersDao, atLeastOnce()).authorize(eq("user"), eq("a94a8fe5ccb19ba61c4c0873d391e987982fbbd3"));
	}
}