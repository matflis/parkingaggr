package pl.matflis.parkingweb.server.integration;

import org.junit.BeforeClass;
import org.junit.Test;
import pl.matflis.parkingweb.core.common.Position;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionDTO;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionEntryInfo;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionSubmissionDTO;
import pl.matflis.parkingweb.core.subscriptions.model.ViolationType;
import pl.matflis.parkingweb.core.util.SerDeMapper;

import java.net.HttpURLConnection;

import static org.junit.Assert.assertEquals;

public class ChargeServerTest extends NonPlateRecognitionTest {

	private static final String CHARGE_ENDPOINT = "subscriptions/charge";

	@BeforeClass
	public static void initCharge() {
		initDbScripts("parkingweb_int_charges", "charges.sql");
	}

	@Test
	public void shouldReturnNotFoundForBlankUser() throws Exception {
		Position position = new Position(1d, 1d);
		SubscriptionEntryInfo subscriptionEntry = getSubscriptionEntryInfo();
		SubscriptionSubmissionDTO dto = new SubscriptionSubmissionDTO("", subscriptionEntry, position);

		verifyAction(dto, HttpURLConnection.HTTP_BAD_REQUEST, "Username was not specified");
	}

	@Test
	public void shouldReturnConflictIfUserHasSubscription() throws Exception {
		Position position = new Position(1d, 1d);
		SubscriptionEntryInfo subscriptionEntry = getSubscriptionEntryInfo();
		SubscriptionSubmissionDTO dto = new SubscriptionSubmissionDTO("test", subscriptionEntry, position);

		verifyAction(dto, HttpURLConnection.HTTP_CONFLICT, "Cannot give a ticket. Registration plate has subscription bought");
	}

	@Test
	public void shouldReturnNotFoundIfUserDoesNotExist() throws Exception {
		Position position = new Position(1d, 1d);
		SubscriptionEntryInfo subscriptionEntry = getSubscriptionEntryInfo("WWA 123");
		SubscriptionSubmissionDTO dto = new SubscriptionSubmissionDTO("test2", subscriptionEntry, position);

		verifyAction(dto, HttpURLConnection.HTTP_NOT_FOUND, "User with name 'test2' does not exist");
	}

	@Test
	public void shouldReturnOkIfUserChargedSuccessfully() throws Exception {
		Position position = new Position(1d, 1d);
		SubscriptionEntryInfo subscriptionEntry = getSubscriptionEntryInfo("WWA 123");
		SubscriptionSubmissionDTO dto = new SubscriptionSubmissionDTO("test", subscriptionEntry, position);

		verifyAction(dto, HttpURLConnection.HTTP_OK, "Ticket given successfully");
	}

	private void verifyAction(SubscriptionSubmissionDTO dto, int expectedCode, String expectedMessage) throws Exception {
		ServerResponse response = doTest(CHARGE_ENDPOINT, SerDeMapper.toJson(dto).getBytes());

		assertEquals(expectedCode, response.getStatus());
		assertEquals(expectedMessage, response.getContent());
	}

	private SubscriptionEntryInfo getSubscriptionEntryInfo() {
		return getSubscriptionEntryInfo("KRK 123");
	}

	private SubscriptionEntryInfo getSubscriptionEntryInfo(String plate) {
		SubscriptionDTO subscriptionDTO = new SubscriptionDTO.SubscriptionDTOBuilder()
				.withStreetName("street")
				.withRegistrationNumber(plate)
				.withBrand("OPEL")
				.withViolationType(ViolationType.LACK_OF_TICKET)
				.withTicketAmount(50)
				.build();
		return new SubscriptionEntryInfo(subscriptionDTO);
	}
}