package pl.matflis.parkingweb.server.integration;

import org.apache.commons.io.IOUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import pl.matflis.parkingweb.server.db.PropertiesSource;
import pl.matflis.parkingweb.server.server.ServerMain;
import ratpack.test.MainClassApplicationUnderTest;
import ratpack.test.ServerBackedApplicationUnderTest;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;

import static org.mockito.ArgumentMatchers.eq;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"javax.management.*", "javax.net.ssl.*", "javax.security.*"})
@PrepareForTest(value = PropertiesSource.class)
abstract class BaseServerTest {

	@Rule
	public NonValidatingSSLClientContext nonValidatingSSLClientContext = new NonValidatingSSLClientContext();

	private static ServerBackedApplicationUnderTest app;

	@BeforeClass
	public static void init() {
		mockStatic(PropertiesSource.class);
		when(PropertiesSource.getValue(eq(PropertiesSource.Property.JDBC_DRIVER_CLASS)))
				.thenReturn("org.h2.Driver");
		when(PropertiesSource.getValue(eq(PropertiesSource.Property.JDBC_USER)))
				.thenReturn("root");
		when(PropertiesSource.getValue(eq(PropertiesSource.Property.JDBC_PASSWORD)))
				.thenReturn("sa");
		app = new MainClassApplicationUnderTest(ServerMain.class);
	}

	static void initDbScripts(String dbName, String source) {
		final String jdbcUrlTemplate = "jdbc:h2:mem:%s;INIT=RUNSCRIPT FROM 'classpath:scripts/init.sql'\\;" +
				"RUNSCRIPT FROM 'classpath:integration/%s'";
		when(PropertiesSource.getValue(eq(PropertiesSource.Property.JDBC_URL)))
				.thenReturn(String.format(jdbcUrlTemplate, dbName, source));
	}

	@AfterClass
	public static void tearDown() {
		app.close();
	}

	ServerResponse doTest(String endpoint, byte[] content) {
		try {
			final URL remoteUrl = new URL(app.getAddress().toURL().toString() + endpoint);
			final HttpsURLConnection remoteConnection = (HttpsURLConnection) remoteUrl.openConnection();
			remoteConnection.setDoOutput(true);
			remoteConnection.setChunkedStreamingMode(0);

			try {
				try (OutputStream out = new BufferedOutputStream(remoteConnection.getOutputStream())) {
					out.write(content);
				}

				final int responseCode = remoteConnection.getResponseCode();
				if (responseCode != HttpURLConnection.HTTP_OK) {
					return new ServerResponse(responseCode, getStreamContent(remoteConnection.getErrorStream()));
				}
				// Read response
				final String responseContent = getStreamContent(remoteConnection.getInputStream());
				return new ServerResponse(responseCode, responseContent);
			} finally {
				remoteConnection.disconnect();
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private String getStreamContent(InputStream stream) throws IOException {
		return IOUtils.toString(stream, Charset.defaultCharset());
	}

	static class ServerResponse {
		final int status;
		final String content;

		ServerResponse(int status, String content) {
			this.status = status;
			this.content = content;
		}

		int getStatus() {
			return status;
		}

		String getContent() {
			return content;
		}
	}
}