package pl.matflis.parkingweb.server.integration;

import org.apache.commons.io.IOUtils;
import org.junit.Rule;
import org.junit.Test;
import org.powermock.core.classloader.annotations.PowerMockIgnore;

import java.net.HttpURLConnection;

import static org.junit.Assert.assertEquals;

@PowerMockIgnore({"org.w3c.dom.*", "javax.xml.*", "org.xml.*"})
public class RecognisePlateServerTest extends BaseServerTest {

	private static final String RECOGNISE_ENDPOINT = "subscriptions/recognise";

	@Rule
	public NonValidatingSSLClientContext rule = new NonValidatingSSLClientContext();

	@Test
	public void shouldProperlyParsePlate() throws Exception {
		verifyPictureRecognition("car", HttpURLConnection.HTTP_OK, "LM298AI");
	}

	@Test
	public void shouldNotRecognisePlate() throws Exception {
		verifyPictureRecognition("monkey", HttpURLConnection.HTTP_NOT_FOUND, "Cannot find plate from picture");
	}

	private void verifyPictureRecognition(String image, int status, String responseMessage) throws Exception {
		byte[] imageBytes = IOUtils.toByteArray(getClass().getClassLoader()
				.getResourceAsStream("images/" + image + ".jpg"));
		final ServerResponse response = doTest(RECOGNISE_ENDPOINT, imageBytes);

		assertEquals(status, response.getStatus());
		assertEquals(responseMessage, response.getContent());
	}
}