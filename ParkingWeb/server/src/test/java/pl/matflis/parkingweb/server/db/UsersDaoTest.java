package pl.matflis.parkingweb.server.db;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;


public class UsersDaoTest extends BaseDaoTest {

	private final UsersDao usersDao = UsersDao.getInstance();

	@BeforeClass
	public static void init() {
		initDbScripts("parkingweb_users", "users_init.sql");
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	public void shouldFindUserIdForGivenUsername() {
		final Optional<Long> userId = usersDao.getUserId("test.user");
		assertEquals(Long.valueOf(1L), userId.get());
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	public void shouldFindUserIdCaseInsensitively() {
		final Optional<Long> userId = usersDao.getUserId("TEST.USER4");
		assertEquals(Long.valueOf(4L), userId.get());
	}

	@Test
	public void shouldNotFindUserWithNonExistingUsername() {
		assertFalse(usersDao.getUserId("non.existing").isPresent());
	}

	@Test
	public void shouldNotAuthorizeNonExistingUser() {
		final boolean result = usersDao.authorize("login.missing", "password");
		assertEquals(false, result);
	}

	@Test
	public void shouldNotAuthorizeUserWithWrongPassword() {
		final boolean result = usersDao.authorize("login.invalid", "test-100");
		assertEquals(false, result);
	}

	@Test
	public void shouldAuthorizeUserWithValidCredentials() {
		final boolean result = usersDao.authorize("login.valid", "109f4b3c50d7b0df729d299bc6f8e9ef9066971f");
		assertEquals(true, result);
	}
}