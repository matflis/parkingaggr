package pl.matflis.parkingweb.server.integration;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public class NonValidatingSSLClientContext implements TestRule {
	@Override
	public Statement apply(Statement base, Description description) {
		return new Statement() {
			@Override
			public void evaluate() throws Throwable {

				String originalSocketFactoryProvider = Security.getProperty("ssl.SocketFactory.provider");
				final HostnameVerifier originalHostnameVerifier = HttpsURLConnection.getDefaultHostnameVerifier();
				Security.setProperty("ssl.SocketFactory.provider", DummySSLSocketFactory.class.getName());
				HttpsURLConnection.setDefaultHostnameVerifier(ALLOW_ALL_HOSTNAME_VERIFIER);
				try {
					base.evaluate();
				} finally {
					originalSocketFactoryProvider = originalSocketFactoryProvider != null ? originalSocketFactoryProvider : "";
					Security.setProperty("ssl.SocketFactory.provider", originalSocketFactoryProvider);
					HttpsURLConnection.setDefaultHostnameVerifier(originalHostnameVerifier);
				}
			}
		};
	}

	public static final HostnameVerifier ALLOW_ALL_HOSTNAME_VERIFIER = new HostnameVerifier() {
		@Override
		public boolean verify(String s, SSLSession sslSession) {
			return true;
		}
	};

	public static final TrustManager TRUST_ALL_TRUST_MANAGER = new X509TrustManager() {
		@Override
		public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

		}

		@Override
		public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

		}

		@Override
		public X509Certificate[] getAcceptedIssuers() {
			return null;
		}
	};

	public static class DummySSLSocketFactory extends SSLSocketFactory {

		private final SSLContext sslContext = SSLContext.getInstance("TLS");
		private final SSLSocketFactory factory;

		public DummySSLSocketFactory() throws NoSuchAlgorithmException, KeyManagementException {
			sslContext.init(null, new TrustManager[]{TRUST_ALL_TRUST_MANAGER}, new SecureRandom());
			factory = sslContext.getSocketFactory();
		}

		@Override
		public Socket createSocket() throws IOException {
			return factory.createSocket();
		}

		@Override
		public Socket createSocket(InetAddress address, int port, InetAddress localAddress, int localPort) throws
				IOException {
			return factory.createSocket(address, port, localAddress, localPort);
		}

		@Override
		public Socket createSocket(InetAddress host, int port) throws IOException {
			return factory.createSocket(host, port);
		}

		@Override
		public Socket createSocket(Socket s, String host, int port, boolean autoClose) throws IOException {
			return factory.createSocket(s, host, port, autoClose);
		}

		@Override
		public Socket createSocket(String host, int port, InetAddress localHost, int localPort) throws IOException {
			return factory.createSocket(host, port, localHost, localPort);
		}

		@Override
		public Socket createSocket(String host, int port) throws IOException {
			return factory.createSocket(host, port);
		}

		@Override
		public String[] getDefaultCipherSuites() {
			return factory.getDefaultCipherSuites();
		}

		@Override
		public String[] getSupportedCipherSuites() {
			return factory.getSupportedCipherSuites();
		}
	}
}