package pl.matflis.parkingweb.server.db;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.BeforeClass;
import org.junit.Test;
import org.sql2o.ResultSetHandler;
import org.sql2o.Sql2o;
import org.sql2o.Sql2oException;
import pl.matflis.parkingweb.core.common.Position;
import pl.matflis.parkingweb.core.subscriptions.model.CountryCode;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionDTO;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionEntryInfo;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionSubmissionDTO;
import pl.matflis.parkingweb.core.subscriptions.model.ViolationType;
import pl.matflis.parkingweb.server.model.ChargeApplyInfo;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static pl.matflis.parkingweb.server.model.ChargeApplyInfo.ChargeApplyInfoBuilder;

public class ChargeDaoTest extends BaseDaoTest {

	private final ChargeDao chargeDao = ChargeDao.getInstance();

	@BeforeClass
	public static void init() {
		initDbScripts("parkingweb_charges", "charges_init.sql");
	}

	@Test(expected = Sql2oException.class)
	public void shouldThrowExceptionWhenInsertingChargeWithNonExistingUser() {
		ChargeApplyInfoBuilder chargeApplyBuilder = getSubmissionBuilder();
		chargeApplyBuilder.withUserId(3121L);
		chargeDao.charge(chargeApplyBuilder.build());
	}

	@Test
	public void shouldInsertChargeIntoDb() throws NoSuchFieldException, IllegalAccessException {
		ChargeApplyInfoBuilder chargeApplyBuilder = getSubmissionBuilder();
		chargeApplyBuilder.withUserId(1L);
		ChargeApplyInfo chargeApplyInfo = chargeApplyBuilder.build();
		chargeDao.charge(chargeApplyInfo);


		Sql2o value = getDAO(chargeDao);
		final SubscriptionSubmissionDTO dbSubscriptionSubmissionDTO = value.open()
				.createQuery("SELECT * FROM Charges")
				.executeAndFetchFirst(new SubscriptionSubmissionResultHandler());

		assertEquals(chargeApplyInfo.getLongitude(), dbSubscriptionSubmissionDTO.getPosition().getLongitude(), 0);
		assertEquals(chargeApplyInfo.getLatitude(), dbSubscriptionSubmissionDTO.getPosition().getLatitude(), 0);

		final SubscriptionEntryInfo dbSubscriptionEntry = dbSubscriptionSubmissionDTO.getSubscriptionEntryInfo();
		assertEquals(chargeApplyInfo.getOffenceTimestamp(), dbSubscriptionEntry.getOffenceTime().getMillis());

		final SubscriptionDTO dbSubscription = dbSubscriptionEntry.getSubscriptionDTO();

		assertEquals(chargeApplyInfo.getStreetName(), dbSubscription.getStreetName());
		assertEquals(chargeApplyInfo.getRegistrationPlate(), dbSubscription.getRegistrationNumber());
		assertEquals(chargeApplyInfo.getCountryCode(), dbSubscription.getCountryCode().name());
		assertEquals(chargeApplyInfo.getBrand(), dbSubscription.getBrand());
		assertEquals(chargeApplyInfo.getRemarks(), dbSubscription.getRemarks());
		assertEquals(chargeApplyInfo.getViolationType(), dbSubscription.getViolationType().getDescription());
		assertEquals(chargeApplyInfo.getTicketAmount(), dbSubscription.getTicketAmount());
	}

	private ChargeApplyInfoBuilder getSubmissionBuilder() {
		return new ChargeApplyInfoBuilder()
				.withStreetName("street")
				.withRegistrationPlate("KRK 123")
				.withCountryCode(CountryCode.PL.name())
				.withBrand("Opel")
				.withViolationType(ViolationType.LACK_OF_TICKET.getDescription())
				.withTicketAmount(100L)
				.withLongitude(1d)
				.withLatitude(2d)
				.withOffenceTimestamp(System.currentTimeMillis());
	}

	private class SubscriptionSubmissionResultHandler implements ResultSetHandler<SubscriptionSubmissionDTO> {
		@Override
		public SubscriptionSubmissionDTO handle(ResultSet resultSet) throws SQLException {
			final double longitude = resultSet.getDouble("longitude");
			final double latitude = resultSet.getDouble("latitude");
			Position position = new Position(longitude, latitude);

			final long timestamp = resultSet.getLong("offenceTimestamp");
			SubscriptionDTO subscriptionDto = new SubscriptionDTO.SubscriptionDTOBuilder()
					.withStreetName(resultSet.getString("StreetName"))
					.withRegistrationNumber(resultSet.getString("RegistrationPlate"))
					.withCountryCode(CountryCode.fromString(resultSet.getString("CountryCode")))
					.withBrand(resultSet.getString("Brand"))
					.withRemarks(resultSet.getString("Remarks"))
					.withViolationType(ViolationType.fromDescription(resultSet.getString("ViolationType")))
					.withTicketAmount(resultSet.getInt("TicketAmount"))
					.build();

			SubscriptionEntryInfo entryInfo = new SubscriptionEntryInfo(subscriptionDto, null, new DateTime(timestamp, DateTimeZone.UTC));
			return new SubscriptionSubmissionDTO(null, entryInfo, position);
		}
	}
}