package pl.matflis.parkingweb.server.db;

import org.junit.BeforeClass;
import org.junit.Test;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionInfo;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionType;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class SubscriptionDaoTest extends BaseDaoTest {

	private final SubscriptionDao subscriptionDao = SubscriptionDao.getInstance();

	@BeforeClass
	public static void init() {
		initDbScripts("parkingweb_subs", "subs_init.sql");
	}

	@Test
	public void shouldFindValidSubscription() {
		final long count = subscriptionDao.countWithPlate("WWA 123", 1L);
		assertEquals(1L, count);
	}

	@Test
	public void shouldNotFindExpiredSubscription() {
		final long count = subscriptionDao.countWithPlate("KRK 123", 1L);
		assertEquals(0L, count);
	}

	@Test
	public void shouldNotFindSubscriptionInDifferentZone() {
		final long count = subscriptionDao.countWithPlate("WWA 123", 2L);
		assertEquals(0L, count);
	}

	@Test
	public void shouldNotFindNotPresentPlate() {
		final long count = subscriptionDao.countWithPlate("NON EXISTING", 1L);
		assertEquals(0L, count);
	}

	@Test
	public void shouldFindValidSubscriptionCaseInsensitively() {
		final long count = subscriptionDao.countWithPlate("wwa 123", 1L);
		assertEquals(1L, count);
	}

	@Test
	public void shouldReturnTicketSubscription() {
		SubscriptionInfo subscriptionType = subscriptionDao.getSubscriptionType("gdA 123");

		assertEquals(SubscriptionType.TICKET, subscriptionType.getSubscriptionType());
		assertEquals(2510523152754L, subscriptionType.getValidTill().getMillis());
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	public void shouldReturnExpirationTimeForValidPlateInExistingZone() {
		Optional<Long> expirationTime = subscriptionDao.getExpirationTime("lub 312", 1L);

		assertEquals(Long.valueOf(1510518611L), expirationTime.get());
	}

	@Test
	public void shouldNotFindExpirationDateForPlateInDifferentZone() {
		Optional<Long> expirationTime = subscriptionDao.getExpirationTime("lub 312", 2L);

		assertEquals(Optional.empty(), expirationTime);
	}

	@Test
	public void shouldReturnContractSubscription() {
		SubscriptionInfo subscriptionType = subscriptionDao.getSubscriptionType("Rze 123");

		assertEquals(SubscriptionType.SUBSCRIPTION, subscriptionType.getSubscriptionType());
		assertEquals(2510523152754L, subscriptionType.getValidTill().getMillis());
	}

	@Test
	public void shouldReturnNoSubscription() {
		SubscriptionInfo subscriptionType = subscriptionDao.getSubscriptionType("Silly plate");

		assertEquals(SubscriptionType.NO_SUBSCRIPTION, subscriptionType.getSubscriptionType());
		assertNull(subscriptionType.getValidTill());
	}
}