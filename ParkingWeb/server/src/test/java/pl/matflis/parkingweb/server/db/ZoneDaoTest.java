package pl.matflis.parkingweb.server.db;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class ZoneDaoTest extends BaseDaoTest {

	private final ZoneDao zoneDao = ZoneDao.getInstance();

	@BeforeClass
	public static void init() {
		initDbScripts("parkingweb_zones", "zones_init.sql");
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	public void shouldFindZoneForGivenStreet() {
		// Given
		String street = "flisowska";

		// When
		Optional<Long> zone = zoneDao.getZoneNumber(street);

		// Then
		assertEquals(Long.valueOf(1L), zone.get());
	}

	@Test
	public void shouldNotFindZoneWhenStreetDoesNotExist() {
		// Given
		String street = "SomeNonExisting";

		// When
		Optional<Long> zone = zoneDao.getZoneNumber(street);

		// Then
		assertFalse(zone.isPresent());
	}
}