package pl.matflis.parkingweb.server.db;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.sql2o.Sql2o;

import java.lang.reflect.Field;

import static org.mockito.ArgumentMatchers.eq;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
@PrepareForTest(value = PropertiesSource.class)
public abstract class BaseDaoTest {

	@BeforeClass
	public static void initBase() {
		mockStatic(PropertiesSource.class);
		when(PropertiesSource.getValue(eq(PropertiesSource.Property.JDBC_DRIVER_CLASS)))
				.thenReturn("org.h2.Driver");
		when(PropertiesSource.getValue(eq(PropertiesSource.Property.JDBC_USER)))
				.thenReturn("root");
		when(PropertiesSource.getValue(eq(PropertiesSource.Property.JDBC_PASSWORD)))
				.thenReturn("sa");
	}

	static void initDbScripts(String dbName, String source) {
		final String jdbcUrlTemplate = "jdbc:h2:mem:%s;INIT=RUNSCRIPT FROM 'classpath:scripts/init.sql'\\;" +
				"RUNSCRIPT FROM 'classpath:scripts/%s'";
		when(PropertiesSource.getValue(eq(PropertiesSource.Property.JDBC_URL)))
				.thenReturn(String.format(jdbcUrlTemplate, dbName, source));
	}

	Sql2o getDAO(Object subclassDao) throws NoSuchFieldException, IllegalAccessException {
		Field field = subclassDao.getClass().getSuperclass().getDeclaredField("sql2o");
		field.setAccessible(true);
		return (Sql2o) field.get(subclassDao);
	}
}