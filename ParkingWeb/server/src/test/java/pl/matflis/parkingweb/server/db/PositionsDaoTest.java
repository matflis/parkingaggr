package pl.matflis.parkingweb.server.db;

import org.junit.BeforeClass;
import org.junit.Test;
import org.sql2o.Connection;
import org.sql2o.Sql2o;
import org.sql2o.Sql2oException;
import pl.matflis.parkingweb.core.common.Position;

import static org.junit.Assert.assertEquals;

public class PositionsDaoTest extends BaseDaoTest {

	private final PositionsDao positionsDao = PositionsDao.getInstance();

	@BeforeClass
	public static void init() {
		initDbScripts("parkingweb_positions", "positions_init.sql");
	}

	@Test
	public void shouldInsertPositionToDb() throws NoSuchFieldException, IllegalAccessException {
		// Given
		final Position position = new Position(1d, 1d);
		final long userId = 1L;

		// When
		positionsDao.insert(position, userId);

		// Then
		final Sql2o dao = getDAO(positionsDao);
		try (Connection connection = dao.open()) {
			final long count = connection.createQuery("SELECT COUNT(*) FROM Positions " +
					"WHERE UserId = :userId")
					.addParameter("userId", userId)
					.executeAndFetchFirst(Long.class);
			assertEquals(1L, count);
		}
	}

	@Test(expected = Sql2oException.class)
	public void shouldNotInsertPositionIntoDbWhenUserNotExists() {
		// Given
		final Position position = new Position(1d, 1d);
		final long userId = 42;

		positionsDao.insert(position, userId);
	}
}