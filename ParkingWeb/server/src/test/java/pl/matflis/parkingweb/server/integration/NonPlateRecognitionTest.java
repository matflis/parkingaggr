package pl.matflis.parkingweb.server.integration;

import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;

@SuppressStaticInitializationFor("pl.matflis.parkingweb.server.handler.subscriptions.RecognisePlateHandler")
abstract class NonPlateRecognitionTest extends BaseServerTest {
}