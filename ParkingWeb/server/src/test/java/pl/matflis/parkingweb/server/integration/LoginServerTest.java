package pl.matflis.parkingweb.server.integration;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import pl.matflis.parkingweb.core.auth.UserPasswordData;
import pl.matflis.parkingweb.core.util.SerDeMapper;

public class LoginServerTest extends NonPlateRecognitionTest {

	private static final String LOGIN_ENDPOINT = "login";

	@BeforeClass
	public static void initCharge() {
		initDbScripts("parkingweb_int_users", "users.sql");
	}


	@Test
	public void shouldAuthorizeWithValidCredentials() throws JsonProcessingException {
		final UserPasswordData credentials = new UserPasswordData("login.valid", "test2");
		final ServerResponse response = doTest(LOGIN_ENDPOINT, SerDeMapper.toJson(credentials).getBytes());

		Assert.assertEquals(200, response.getStatus());
	}
}