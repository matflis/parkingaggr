package pl.matflis.parkingweb.server.integration;

import org.joda.time.DateTime;
import org.junit.BeforeClass;
import org.junit.Test;
import pl.matflis.parkingweb.core.subscriptions.model.CheckSubscriptionDto;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionInfo;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionType;
import pl.matflis.parkingweb.core.util.SerDeMapper;

import java.net.HttpURLConnection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class CheckSubscriptionServerTest extends NonPlateRecognitionTest {
	private static final String CHECK_SUBSCRIPTION_ENDPOINT = "subscriptions/check";

	@BeforeClass
	public static void initCharge() {
		initDbScripts("parkingweb_int_subs", "subs.sql");
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	public void shouldFindTicketSubscription() throws Exception {
		CheckSubscriptionDto subscriptionEntry = new CheckSubscriptionDto("gda 123", "street");
		verifyAction(subscriptionEntry, HttpURLConnection.HTTP_OK, SubscriptionType.TICKET, 2510523152754L);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	public void shouldFindTemporarySubscription() throws Exception {
		CheckSubscriptionDto subscriptionEntry = new CheckSubscriptionDto("rze 123", "street");
		verifyAction(subscriptionEntry, HttpURLConnection.HTTP_OK, SubscriptionType.SUBSCRIPTION, 2510523152754L);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	public void shouldNotFindAnySubscription() throws Exception {
		CheckSubscriptionDto subscriptionEntry = new CheckSubscriptionDto("test 123", "street");
		verifyAction(subscriptionEntry, HttpURLConnection.HTTP_OK, SubscriptionType.NO_SUBSCRIPTION, null);
	}

	private void verifyAction(CheckSubscriptionDto subscriptionEntry, int expectedStatus,
							  SubscriptionType expectedSubscriptionType, Object expectedTime) throws Exception {
		ServerResponse response = doTest(CHECK_SUBSCRIPTION_ENDPOINT, SerDeMapper.toJson(subscriptionEntry).getBytes());

		SubscriptionInfo actual = SerDeMapper.fromJson(response.getContent(), SubscriptionInfo.class);
		assertEquals(expectedStatus, response.getStatus());
		assertEquals(expectedSubscriptionType, actual.getSubscriptionType());
		final DateTime validTill = actual.getValidTill();
		if (expectedTime == null) {
			assertNull(validTill);
		} else {
			assertEquals(expectedTime, validTill.getMillis());
		}
	}
}