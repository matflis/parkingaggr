package pl.matflis.parkingweb.server.integration;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.BeforeClass;
import org.junit.Test;
import pl.matflis.parkingweb.core.common.Position;
import pl.matflis.parkingweb.core.position.PositionReadDTO;
import pl.matflis.parkingweb.core.util.SerDeMapper;

import java.net.HttpURLConnection;

import static org.junit.Assert.assertEquals;

public class PositionUpdateServerTest extends NonPlateRecognitionTest {

	private static final String POSITIONS_ENDPOINT = "positions";

	@BeforeClass
	public static void initPositions() {
		initDbScripts("parkingweb_int_position", "positions.sql");
	}

	@Test
	public void shouldAddPosition() throws JsonProcessingException {
		// Given
		PositionReadDTO positionReadDTO = getPostData("test");

		// When
		ServerResponse response = doTest(POSITIONS_ENDPOINT, SerDeMapper.toJson(positionReadDTO).getBytes());

		// Then
		assertEquals(HttpURLConnection.HTTP_OK, response.getStatus());
		assertEquals("Position inserted successfully", response.getContent());
	}

	@Test
	public void shouldReturnNotFoundWhenUserDoesNotExist() throws JsonProcessingException {
		// Given
		PositionReadDTO positionReadDTO = getPostData("not.existing");

		// When
		ServerResponse response = doTest(POSITIONS_ENDPOINT, SerDeMapper.toJson(positionReadDTO).getBytes());

		// Then
		assertEquals(HttpURLConnection.HTTP_NOT_FOUND, response.getStatus());
		assertEquals("Username: not.existing not found", response.getContent());
	}

	private PositionReadDTO getPostData(String username) {
		Position position = new Position(1d, 1d);
		return new PositionReadDTO(position, username);
	}
}