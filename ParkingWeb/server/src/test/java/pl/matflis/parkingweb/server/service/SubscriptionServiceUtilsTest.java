package pl.matflis.parkingweb.server.service;

import org.joda.time.DateTime;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SubscriptionServiceUtilsTest {

	@Test
	public void shouldCorrectlyCalculateFine() {
		Long fine = SubscriptionServiceUtils.getFine((long) (System.currentTimeMillis() - 1.5 * 60 * 60 * 1_000));
		assertEquals(Long.valueOf(6L), fine);
	}

	@Test
	public void shouldExpireTodayIfDayIsTheSameDay() {
		DateTime dateTime = new DateTime();
		dateTime.minusHours(3);

		boolean expiredToday = SubscriptionServiceUtils.expiredToday(dateTime.getMillis());

		assertTrue(expiredToday);
	}

	@Test
	public void shouldNotExpireTodayIfTwoDaysAgo() {
		DateTime dateTime = new DateTime().minusDays(2);

		boolean expiredToday = SubscriptionServiceUtils.expiredToday(dateTime.getMillis());

		assertFalse(expiredToday);
	}
}