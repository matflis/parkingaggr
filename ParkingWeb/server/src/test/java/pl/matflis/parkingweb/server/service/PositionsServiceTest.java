package pl.matflis.parkingweb.server.service;


import org.junit.BeforeClass;
import org.junit.Test;
import org.powermock.core.classloader.annotations.PrepareForTest;
import pl.matflis.parkingweb.core.common.Position;
import pl.matflis.parkingweb.core.position.PositionReadDTO;
import pl.matflis.parkingweb.server.db.PositionsDao;
import pl.matflis.parkingweb.server.db.UsersDao;
import pl.matflis.parkingweb.server.exception.UserNotFoundException;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@PrepareForTest({PositionsDao.class, UsersDao.class})
public class PositionsServiceTest extends BaseServiceTest {

	private final PositionsService positionsService = PositionsService.getInstance();

	private static PositionsDao positionsDao = mock(PositionsDao.class);
	private static UsersDao usersDao = mock(UsersDao.class);
	
	@BeforeClass
	public static void init() {
		mockStatic(PositionsDao.class, UsersDao.class);
		when(UsersDao.getInstance()).thenReturn(usersDao);
		when(PositionsDao.getInstance()).thenReturn(positionsDao);
	}

	@Test
	public void shouldAddReadPosition() {
		// Given
		Position position = new Position(1d, 1d);
		PositionReadDTO positionReadDTO = new PositionReadDTO(position, "usr");
		when(usersDao.getUserId(anyString())).thenReturn(Optional.of(1L));

		// When
		positionsService.addPosition(positionReadDTO);

		// Then
		verify(positionsDao).insert(any(), eq(1L));
	}

	@Test(expected = UserNotFoundException.class)
	public void shouldThrowExceptionIfUserNotFound() {
		// Given
		Position position = new Position(1d, 1d);
		PositionReadDTO positionReadDTO = new PositionReadDTO(position, "user");
		when(usersDao.getUserId(anyString())).thenReturn(Optional.empty());

		// When
		positionsService.addPosition(positionReadDTO);

		// Then exception should be thrown
	}
}