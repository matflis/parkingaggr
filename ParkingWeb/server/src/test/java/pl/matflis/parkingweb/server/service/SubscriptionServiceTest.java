package pl.matflis.parkingweb.server.service;

import org.junit.BeforeClass;
import org.junit.Test;
import org.powermock.core.classloader.annotations.PrepareForTest;
import pl.matflis.parkingweb.core.common.Position;
import pl.matflis.parkingweb.core.subscriptions.model.ChargeResult;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionDTO;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionDTO.SubscriptionDTOBuilder;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionEntryInfo;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionInfo;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionSubmissionDTO;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionType;
import pl.matflis.parkingweb.core.subscriptions.model.ViolationType;
import pl.matflis.parkingweb.server.db.ChargeDao;
import pl.matflis.parkingweb.server.db.SubscriptionDao;
import pl.matflis.parkingweb.server.db.UsersDao;
import pl.matflis.parkingweb.server.db.ZoneDao;
import pl.matflis.parkingweb.server.exception.NotInZoneException;
import pl.matflis.parkingweb.server.exception.SubscriptionAlreadyExistsException;
import pl.matflis.parkingweb.server.exception.UserNotFoundException;
import pl.matflis.parkingweb.server.exception.UserNotSpecifiedException;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

@PrepareForTest({SubscriptionDao.class, UsersDao.class, ChargeDao.class, ZoneDao.class})
public class SubscriptionServiceTest extends BaseServiceTest {

	private static SubscriptionDao subscriptionDao = mock(SubscriptionDao.class);
	private static UsersDao usersDao = mock(UsersDao.class);
	private static ChargeDao chargeDao = mock(ChargeDao.class);
	private static ZoneDao zoneDao = mock(ZoneDao.class);

	private final SubscriptionService subscriptionService = SubscriptionService.getInstance();

	@BeforeClass
	public static void init() {
		mockStatic(SubscriptionDao.class, UsersDao.class, ChargeDao.class, ZoneDao.class);
		when(SubscriptionDao.getInstance()).thenReturn(subscriptionDao);
		when(UsersDao.getInstance()).thenReturn(usersDao);
		when(ChargeDao.getInstance()).thenReturn(chargeDao);
		when(ZoneDao.getInstance()).thenReturn(zoneDao);
	}

	@Test(expected = UserNotSpecifiedException.class)
	public void shouldThrowExceptionIfUserNotSpecified() {
		Position position = new Position(1d, 1d);
		SubscriptionDTO subscriptionDto = mock(SubscriptionDTO.class);
		SubscriptionEntryInfo subscriptionEntry = new SubscriptionEntryInfo(subscriptionDto, null);
		subscriptionService.charge(new SubscriptionSubmissionDTO("", subscriptionEntry, position));
	}

	@Test(expected = SubscriptionAlreadyExistsException.class)
	public void shouldThrowExceptionIfChargedPlateHasSubscription() {
		when(subscriptionDao.countWithPlate(anyString(), anyLong())).thenReturn(1L);
		when(zoneDao.getZoneNumber(any())).thenReturn(Optional.of(1L));

		Position position = new Position(1d, 1d);
		SubscriptionDTO subscriptionDto = getDefaultSubscription();
		SubscriptionEntryInfo subscriptionEntry = new SubscriptionEntryInfo(subscriptionDto, null);
		subscriptionService.charge(new SubscriptionSubmissionDTO("user", subscriptionEntry, position));
	}

	@Test(expected = UserNotFoundException.class)
	public void shouldThrowExceptionIfGivingTicketUserDoesNotExist() {
		when(subscriptionDao.countWithPlate(anyString(), anyLong())).thenReturn(0L);
		when(zoneDao.getZoneNumber(any())).thenReturn(Optional.of(1L));
		when(usersDao.getUserId(anyString())).thenReturn(Optional.empty());

		Position position = new Position(1d, 1d);
		SubscriptionDTO subscriptionDto = getDefaultSubscription();
		SubscriptionEntryInfo subscriptionEntry = new SubscriptionEntryInfo(subscriptionDto, null);
		subscriptionService.charge(new SubscriptionSubmissionDTO("user", subscriptionEntry, position));
	}

	@Test(expected = NotInZoneException.class)
	public void shouldThrowExceptionIfVehicleNotInZone() {
		when(zoneDao.getZoneNumber(any())).thenReturn(Optional.empty());

		Position position = new Position(1d, 1d);
		SubscriptionDTO subscriptionDto = getDefaultSubscription();
		SubscriptionEntryInfo subscriptionEntry = new SubscriptionEntryInfo(subscriptionDto, null);
		subscriptionService.charge(new SubscriptionSubmissionDTO("user", subscriptionEntry, position));
	}

	@Test
	public void shouldAddChargeToDb() {
		// Given
		when(zoneDao.getZoneNumber(any())).thenReturn(Optional.of(1L));
		when(subscriptionDao.countWithPlate(anyString(), anyLong())).thenReturn(0L);
		when(usersDao.getUserId(anyString())).thenReturn(Optional.of(1L));

		Position position = new Position(1d, 1d);
		SubscriptionDTO subscriptionDto = getDefaultSubscription();
		SubscriptionEntryInfo subscriptionEntry = new SubscriptionEntryInfo(subscriptionDto, null);

		// When
		ChargeResult result = subscriptionService.charge(new SubscriptionSubmissionDTO("user", subscriptionEntry, position));

		// Then
		assertEquals(ViolationType.LACK_OF_TICKET, result.getViolationType());
		verify(chargeDao).charge(any());
	}

	@Test
	public void shouldReturnSubscriptionNotNeededIfStreetNotInZone() {
		// Given
		when(zoneDao.getZoneNumber(any())).thenReturn(Optional.empty());
		String street = "Street";
		String registration = "ABC 123";

		// When
		SubscriptionInfo subscriptionInfo = subscriptionService.getSubscriptionInfo(street, registration);

		// Then
		assertEquals(SubscriptionType.SUBSCRIPTION_NOT_NEEDED, subscriptionInfo.getSubscriptionType());
	}

	@Test
	public void shouldReturnSubscriptionTypeIfInZone() {
		// Given
		SubscriptionInfo definedSubscription = new SubscriptionInfo(SubscriptionType.NO_SUBSCRIPTION);
		when(zoneDao.getZoneNumber(any())).thenReturn(Optional.of(1L));
		when(subscriptionDao.getSubscriptionType(any())).thenReturn(definedSubscription);
		String street = "Street";
		String registration = "ABC 123";

		// When
		SubscriptionInfo subscriptionInfo = subscriptionService.getSubscriptionInfo(street, registration);

		// Then
		assertEquals(SubscriptionType.NO_SUBSCRIPTION, subscriptionInfo.getSubscriptionType());
	}

	private SubscriptionDTO getDefaultSubscription() {
		return new SubscriptionDTOBuilder()
				.withStreetName("street")
				.withRegistrationNumber("number")
				.withBrand("brand")
				.withViolationType(ViolationType.LACK_OF_TICKET)
				.withTicketAmount(10)
				.build();
	}
}