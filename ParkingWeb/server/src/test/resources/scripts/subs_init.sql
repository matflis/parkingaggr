INSERT INTO Zones VALUES (1, 'Stare Miasto');

-- Check validity data
INSERT INTO Subscriptions VALUES (1, 'KRK 123', 'TICKET', 1, 1);
INSERT INTO Subscriptions VALUES (2, 'WWA 123', 'SUBSCRIPTION', 2510523152754, 1);
INSERT INTO Subscriptions VALUES (3, 'LUB 312', 'TICKET', 1510518611, 1);

-- Check subscription type data
INSERT INTO Subscriptions VALUES (4, 'GDA 123', 'TICKET', 2510523152754, 1);
INSERT INTO Subscriptions VALUES (5, 'RZE 123', 'SUBSCRIPTION', 2510523152754, 1);
