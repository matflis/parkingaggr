package mobile.projects.matflis.parkingmobile.exception;

public class SubscriptionSubmissionException extends ParkingMobileException {

    public SubscriptionSubmissionException(String message) {
        super(message);
    }

    public SubscriptionSubmissionException(String msg, Throwable cause) {
        super(msg, cause);
    }
}