package mobile.projects.matflis.parkingmobile.service.auth;

import mobile.projects.matflis.parkingmobile.activity.LoginActivity;
import mobile.projects.matflis.parkingmobile.service.WebRemoteHandler;
import mobile.projects.matflis.parkingmobile.service.core.ProgressDialogTask;
import mobile.projects.matflis.parkingmobile.service.core.TaskResult;
import pl.matflis.parkingweb.core.auth.UserPasswordData;
import pl.matflis.parkingweb.core.util.SerDeMapper;

public class AuthenticationTask extends ProgressDialogTask<UserPasswordData, String, LoginActivity> {

    private static final String LOGIN_ENDPOINT = "login";

    private final WebRemoteHandler webRemoteHandler = WebRemoteHandler.getInstance();

    public AuthenticationTask(LoginActivity activity) {
        super(activity);
    }

    @Override
    public String getExecuteMessage() {
        return "Uwierzytelniam";
    }

    @Override
    protected String doInBackgroundInternal(UserPasswordData param) throws Exception {
        return webRemoteHandler.post(LOGIN_ENDPOINT, SerDeMapper.toJson(param).getBytes());
    }

    @Override
    protected void onPostExecute(TaskResult<String> result) {
        super.onPostExecute(result);
        if (result.isSuccessful()) {
            activity.onLoginPerformed(true, null);
        } else {
            activity.onLoginPerformed(false, result.getExceptionMessage());
        }
    }
}