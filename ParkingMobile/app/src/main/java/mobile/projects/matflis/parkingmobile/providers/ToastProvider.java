package mobile.projects.matflis.parkingmobile.providers;

import android.content.Context;
import android.widget.Toast;

public class ToastProvider {

    public static void showLongToast(Context context, String message) {
        showMessage(context, message, Toast.LENGTH_LONG);
    }

    public static void showShortToast(Context context, String message) {
        showMessage(context, message, Toast.LENGTH_SHORT);
    }

    private static void showMessage(Context context, String message, int duration) {
        Toast.makeText(context, message, duration).show();
    }
}