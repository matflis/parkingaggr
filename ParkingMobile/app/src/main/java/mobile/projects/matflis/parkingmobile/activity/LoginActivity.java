package mobile.projects.matflis.parkingmobile.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import mobile.projects.matflis.parkingmobile.R;
import mobile.projects.matflis.parkingmobile.config.ParameterManager;
import mobile.projects.matflis.parkingmobile.config.ParameterManager.Parameter;
import mobile.projects.matflis.parkingmobile.service.auth.AuthenticationTask;
import pl.matflis.parkingweb.core.auth.UserPasswordData;

public class LoginActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
    }

    // Handlers

    public void onLoginClicked(View view) {
        final String username = getTextValue(R.id.loginEditText);
        final String password = getTextValue(R.id.passwordEditText);
        final UserPasswordData credentials = new UserPasswordData(username, password);

        AuthenticationTask authenticationTask = new AuthenticationTask(this);
        authenticationTask.execute(credentials);
    }

    public void onLoginPerformed(boolean isSuccessful, String message) {
        if (isSuccessful) {
            final String username = getTextValue(R.id.loginEditText);
            ParameterManager.setParameterValue(Parameter.USERNAME, username);
            Intent intent = new Intent(this, MenuActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
            startActivity(intent);
            this.finish();
        } else {
            final TextView validationTextView = (TextView) findViewById(R.id.validationResTextView);
            validationTextView.setVisibility(View.VISIBLE);
            validationTextView.setText(message);
        }
    }

    @NonNull
    private String getTextValue(int viewId) {
        return ((EditText) findViewById(viewId)).getText().toString();
    }
}