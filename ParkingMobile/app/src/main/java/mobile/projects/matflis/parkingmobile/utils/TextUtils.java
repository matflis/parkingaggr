package mobile.projects.matflis.parkingmobile.utils;

import org.apache.commons.lang.StringUtils;

public final class TextUtils {

    public static String getOrElse(String text, String defaultValue) {
        if (StringUtils.isNotBlank(text)) {
            return text;
        }
        return defaultValue;
    }

    private TextUtils() {
    }
}