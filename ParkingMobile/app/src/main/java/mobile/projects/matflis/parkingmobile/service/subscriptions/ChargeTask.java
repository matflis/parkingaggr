package mobile.projects.matflis.parkingmobile.service.subscriptions;

import android.util.Log;

import com.google.common.base.Optional;

import mobile.projects.matflis.parkingmobile.activity.ChargeActivity;
import mobile.projects.matflis.parkingmobile.providers.ToastProvider;
import mobile.projects.matflis.parkingmobile.service.core.TaskResult;
import mobile.projects.matflis.parkingmobile.service.subscriptions.exception.SubscriptionFileSaveException;
import mobile.projects.matflis.parkingmobile.service.subscriptions.exception.SubscriptionPostException;
import mobile.projects.matflis.parkingmobile.service.subscriptions.model.ChargeSubmit;
import mobile.projects.matflis.parkingmobile.storage.StorageManager;
import pl.matflis.parkingweb.core.subscriptions.model.ChargeResult;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionDTO;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionEntryInfo;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionSubmissionDTO;

public class ChargeTask extends BaseSubscriptionTask<SubscriptionSubmissionDTO, String, ChargeActivity> {

    private static final String TAG = "ChargeTask";

    private SubscriptionsDao subscriptionsDao;

    public ChargeTask(ChargeActivity activity) {
        super(activity);
        this.subscriptionsDao = SubscriptionsDao.get(activity);
    }

    @SuppressWarnings("OptionalGetWithoutIsPresent")
    @Override
    protected String doInBackgroundInternal(SubscriptionSubmissionDTO submissionDto) {
        SubscriptionEntryInfo subscriptionEntryInfo = submissionDto.getSubscriptionEntryInfo();
        SubscriptionDTO subscriptionDTO = subscriptionEntryInfo.getSubscriptionDTO();
        Optional<String> movedFileLocation = StorageManager.move(subscriptionEntryInfo.getFilePath(),
                subscriptionDTO.getUuid());
        if (!movedFileLocation.isPresent()) {
            throw new SubscriptionFileSaveException("Materiał dowodowy nie został zapisany poprawnie");
        }
        try {
            ChargeResult result = subscriptionRemoteHandler.charge(submissionDto);
            SubscriptionEntryInfo dbReadySubscription = new SubscriptionEntryInfo(subscriptionDTO,
                    movedFileLocation.get(), subscriptionEntryInfo.getOffenceTime());
            ChargeSubmit chargeInfo = new ChargeSubmit(subscriptionDTO.getStreetName(),
                    subscriptionDTO.getRegistrationNumber(), subscriptionDTO.getBrand(), subscriptionDTO.getRemarks(),
                    result.getViolationType().getDescription(), String.valueOf(result.getChargeAmount()),
                    movedFileLocation.get(), subscriptionEntryInfo.getOffenceTime().getMillis());
            subscriptionsDao.addCharge(chargeInfo);
            return "Mandat został nałożony";
        } catch (Exception e) {
            String message = "Błąd podczas wysyłania danych: " + e.getMessage();
            Log.e(TAG, message, e);
            StorageManager.moveTo(movedFileLocation.get(), subscriptionEntryInfo.getFilePath());
            throw new SubscriptionPostException(message, e);
        }
    }

    @Override
    protected void onPostExecute(TaskResult<String> result) {
        super.onPostExecute(result);
        if (result.isSuccessful()) {
            ToastProvider.showLongToast(activity, result.getResult());
            activity.finish();
        } else {
            showError(result);
        }
    }

    @Override
    public String getExecuteMessage() {
        return "Wysyłam dane na serwer";
    }
}