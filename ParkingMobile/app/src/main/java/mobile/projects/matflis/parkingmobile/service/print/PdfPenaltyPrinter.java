package mobile.projects.matflis.parkingmobile.service.print;

import android.support.annotation.NonNull;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import org.joda.time.DateTimeZone;

import java.io.FileOutputStream;

import mobile.projects.matflis.parkingmobile.utils.TextUtils;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionDTO;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionEntryInfo;

public final class PdfPenaltyPrinter implements PenaltyWriter {

    private static final String OFFENCE_TIME_FORMAT = "yyyy-MM-dd HH:mm";
    private static final PdfPenaltyPrinter INSTANCE = new PdfPenaltyPrinter();

    public static PdfPenaltyPrinter getInstance() {
        return INSTANCE;
    }

    private PdfPenaltyPrinter() {
    }

    @Override
    public void print(SubscriptionEntryInfo subscription, String destinationPath) throws Exception {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(destinationPath));

        document.open();
        Chunk headerChunk = getHeaderChunk();
        document.add(createCenteredParagraph(headerChunk));

        Chunk titleChunk = getCompanyChunk();
        document.add(createCenteredParagraph(titleChunk));

        Chunk infoChunk = new Chunk("Wezwanie do wniesienia dodatkowej oplaty za postoj w strefie abonamentowej\n");
        document.add(createCenteredParagraph(infoChunk));

        Chunk numberChunk = new Chunk(String.format("Numer %s%n%n", subscription.getSubscriptionDTO().getUuid()));
        document.add(createCenteredParagraph(numberChunk));

        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(100);
        table.addCell(buildCell("Data zdarzenia:"));
        table.addCell(buildCell(subscription.getOffenceTime().toDateTime(DateTimeZone.forOffsetHours(2))
                .toString(OFFENCE_TIME_FORMAT)));

        SubscriptionDTO subscriptionDTO = subscription.getSubscriptionDTO();
        table.addCell(buildCell("Ulica:"));
        table.addCell(buildCell(subscriptionDTO.getStreetName()));

        table.addCell(buildCell("Szczegoly:"));
        table.addCell(buildCell(subscriptionDTO.getBrand() + ", " + subscriptionDTO.getRegistrationNumber()));

        table.addCell(buildCell("Uwagi:"));
        table.addCell(buildCell(TextUtils.getOrElse(subscriptionDTO.getRemarks(), "Brak")));
        table.addCell(buildCell("Wykroczenie:"));
        table.addCell(buildCell(subscriptionDTO.getViolationType().getDescription() + " - " + subscriptionDTO.getTicketAmount() + " zl"));
        document.add(table);

        document.close();
    }

    @NonNull
    private PdfPCell buildCell(String content) {
        PdfPCell cell = new PdfPCell(new Phrase(content));
        cell.setPaddingTop(10);
        cell.setPaddingBottom(10);
        cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }

    @NonNull
    private Chunk getHeaderChunk() {
        Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
        return new Chunk("Obsluga parkingowa PARKINGEX\n", font);
    }

    @NonNull
    private Chunk getCompanyChunk() {
        return new Chunk("ul. Testowa 1a\n00-001 Warszawa\ntel. 123 456 789\n\n");
    }

    private Element createCenteredParagraph(Chunk chunk) {
        Paragraph paragraph = new Paragraph(chunk);
        paragraph.setAlignment(Element.ALIGN_CENTER);
        return paragraph;
    }
}