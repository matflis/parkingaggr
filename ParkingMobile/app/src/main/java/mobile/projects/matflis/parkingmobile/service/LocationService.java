package mobile.projects.matflis.parkingmobile.service;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;

import com.google.common.base.Optional;

import pl.matflis.parkingweb.core.common.Position;

import static java.lang.String.format;

public class LocationService implements LocationListener {

    private static final String TAG = "LocationService";

    private final static LocationService locationService = new LocationService();

    private Position position;

    private LocationService() {
    }

    public static LocationService get() {
        return locationService;
    }

    public Optional<Position> getPositionOptional() {
        return Optional.fromNullable(position);
    }

    @Override
    public void onLocationChanged(Location location) {
        this.position = new Position(location.getLongitude(), location.getLatitude());
        Log.i(TAG, format("Received position: %s", position));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.i(TAG, format("Provider status changed - Provider: %s, status: %s", provider, status));
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d(TAG, format("Provider %s has been enabled", provider));
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d(TAG, format("Provider %s has been disabled", provider));
    }
}