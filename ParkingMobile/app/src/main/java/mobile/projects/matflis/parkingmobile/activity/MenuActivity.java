package mobile.projects.matflis.parkingmobile.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import java.util.Arrays;

import mobile.projects.matflis.parkingmobile.R;
import mobile.projects.matflis.parkingmobile.config.ParameterManager;
import mobile.projects.matflis.parkingmobile.config.ParameterManager.Parameter;
import mobile.projects.matflis.parkingmobile.constants.Codes;
import mobile.projects.matflis.parkingmobile.service.LocationService;
import mobile.projects.matflis.parkingmobile.service.PositionUpdateService;
import mobile.projects.matflis.parkingmobile.utils.PermissionProviderUtil;

import static mobile.projects.matflis.parkingmobile.config.ParameterManager.Parameter.SERVER_URL;
import static mobile.projects.matflis.parkingmobile.config.ParameterManager.Parameter.USERNAME;


public class MenuActivity extends AppCompatActivity {

    private static final String TAG = "MenuActivity";

    private final LocationService locationService = LocationService.get();
    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_activity);

        setSupportActionBar((Toolbar) findViewById(R.id.mainMenuToolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        initLocationManager();
    }

    private void initLocationManager() {
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!isAllowed(Manifest.permission.ACCESS_FINE_LOCATION) && !isAllowed(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                String[] permissions = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
                requestPermissions(permissions, Codes.LOCATION_REQUEST_CODE);
                Log.d(TAG, "Requested for location permissions");
            } else {
                if (isAllowed(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    addLocationUpdates(LocationManager.GPS_PROVIDER);
                }
                if (isAllowed(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    addLocationUpdates(LocationManager.NETWORK_PROVIDER);
                }
                startUpdateService();
            }
        } else {
            addLocationUpdates(LocationManager.GPS_PROVIDER, LocationManager.NETWORK_PROVIDER);
            Log.d(TAG, "Build version < 24 - setting location updates");
            startUpdateService();
        }
    }

    private boolean isAllowed(String permission) {
        return PermissionProviderUtil.isAllowed(this, permission);
    }

    private void addLocationUpdates(String... providers) {
        for (String provider : providers) {
            locationManager.requestLocationUpdates(provider, 1, 0, locationService);
            Log.d(TAG, String.format("Launching %s Location Provider", provider));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == Codes.LOCATION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                int permittedLocations = 0;
                if (hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION, permissions, grantResults)) {
                    addLocationUpdates(LocationManager.NETWORK_PROVIDER);
                    permittedLocations++;
                }
                if (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION, permissions, grantResults)) {
                    addLocationUpdates(LocationManager.GPS_PROVIDER);
                    permittedLocations++;
                }
                if (permittedLocations == 0) {
                    finish();
                } else {
                    startUpdateService();
                }
            } else {
                // Close application
                finish();
            }
        }
    }

    private void startUpdateService() {
        String username = ParameterManager.getParameterValue(USERNAME);
        PositionUpdateService.start(locationService, username);
    }

    private boolean hasPermission(String permissionName, String[] permissions, int[] grantResults) {
        if (grantResults.length > 0) {
            final int permissionIndex = Arrays.asList(permissions).indexOf(permissionName);
            return grantResults[permissionIndex] == PackageManager.PERMISSION_GRANTED;
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.logoutMenuItem) {
            AlertDialog confirmLogoutDialog = buildConfirmLogoutDialog();
            confirmLogoutDialog.show();
            return true;
        } else if (id == R.id.settingsMenuItem) {
            AlertDialog configureRemoteDialog = buildConfigureRemoteDialog();
            configureRemoteDialog.show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private AlertDialog buildConfirmLogoutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MenuActivity.this);
        builder.setTitle(R.string.logout_title)
                .setMessage(R.string.logout_message)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(MenuActivity.this, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                        startActivity(intent);
                        MenuActivity.this.finish();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Empty handler
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }

    private AlertDialog buildConfigureRemoteDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final LayoutInflater inflater = MenuActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.activity_server_config, null);
        builder.setView(dialogView).setTitle(getString(R.string.configureRemote))
                .setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EditText etSrvAddress = (EditText) dialogView.findViewById(R.id.etSrvConfigAddress);
                        String serverAddress = etSrvAddress.getText().toString();
                        ParameterManager.setParameterValue(Parameter.SERVER_URL, serverAddress);
                    }
                }).setNegativeButton(R.string.cancel, null)
                .setCancelable(false);
        final String currentValue = ParameterManager.getParameterValue(SERVER_URL);
        final EditText etSrvAddress = (EditText) dialogView.findViewById(R.id.etSrvConfigAddress);
        etSrvAddress.setText(currentValue);

        return builder.create();
    }

    // Handlers
    public void onChargeButtonClicked(View view) {
        Intent chargeIntent = new Intent(this, ChargeActivity.class);
        startActivity(chargeIntent);
    }

    public void onCheckSubscriptionClicked(View view) {
        Intent checkSubscriptionIntent = new Intent(this, CheckSubscriptionActivity.class);
        startActivity(checkSubscriptionIntent);
    }

    public void onSeeTicketsButtonClicked(View view) {
        Intent transactionsIntent = new Intent(this, TransactionsActivity.class);
        startActivity(transactionsIntent);
    }
}