package mobile.projects.matflis.parkingmobile.service;

import android.annotation.SuppressLint;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import mobile.projects.matflis.parkingmobile.config.ParameterManager;
import mobile.projects.matflis.parkingmobile.config.ParameterManager.Parameter;
import mobile.projects.matflis.parkingmobile.exception.RemotePostException;

public final class WebRemoteHandler {

    private static final WebRemoteHandler Instance = new WebRemoteHandler();

    static {
        try {
            // Accept only specific certificate with pre-configured digest
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, new TrustManager[]{new TrustSpecificCertificateManager()}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new AcceptSpecificHostnameVerifier());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static WebRemoteHandler getInstance() {
        return Instance;
    }

    private WebRemoteHandler() {
    }

    public String post(String endpoint, byte[] content) throws Exception {
        final String baseRemoteUrl = ParameterManager.getParameterValue(Parameter.SERVER_URL);
        final URL remoteUrl = new URL(baseRemoteUrl + endpoint);
        final HttpsURLConnection remoteConnection = (HttpsURLConnection) remoteUrl.openConnection();
        remoteConnection.setDoOutput(true);
        remoteConnection.setChunkedStreamingMode(0);

        try {
            final OutputStream out = new BufferedOutputStream(remoteConnection.getOutputStream());
            out.write(content);
            out.close();

            // Read response
            final int responseCode = remoteConnection.getResponseCode();
            if (responseCode != HttpURLConnection.HTTP_OK) {
                throw new RemotePostException("Cannot post data. Server respond with status: "
                        + responseCode + ". Reason: " + getStreamContent(remoteConnection.getErrorStream()));
            }

            return getStreamContent(remoteConnection.getInputStream());
        } finally {
            remoteConnection.disconnect();
        }
    }

    private String getStreamContent(InputStream inputStream) throws IOException {
        return IOUtils.toString(inputStream, Charset.defaultCharset());
    }

    private static class TrustSpecificCertificateManager implements X509TrustManager {

        @SuppressLint("TrustAllX509TrustManager")
        @Override
        public void checkClientTrusted(X509Certificate[] chain, String authType) throws
                CertificateException {
        }

        @Override
        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            String acceptableDigest = ParameterManager.getParameterValue(Parameter.CERTIFICATE_DIGEST);
            String calculatedDigest = DigestUtils.sha1Hex(chain[0].getEncoded());
            if (!acceptableDigest.equalsIgnoreCase(calculatedDigest)) {
                throw new CertificateException("Digest of provided certificate does not match!");
            }
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }
    }

    private static class AcceptSpecificHostnameVerifier implements HostnameVerifier {

        @Override
        public boolean verify(String hostname, SSLSession session) {
            String acceptableHost = ParameterManager.getParameterValue(Parameter.SERVER_URL);
            return acceptableHost.contains(hostname);
        }
    }
}