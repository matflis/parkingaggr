package mobile.projects.matflis.parkingmobile.providers;

import android.hardware.Camera;
import android.util.Log;

import com.google.common.base.Optional;

public final class CameraProvider {

    private static final String TAG = "CameraProvider";

    private CameraProvider() {
    }

    @SuppressWarnings("deprecated")
    public static Optional<Camera> get() {
        try {
            Log.d(TAG, "Trying to obtain camera instance");
            final Camera camera = Camera.open();
            if (camera != null) {
                Camera.Parameters parameters = camera.getParameters();
                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                camera.setParameters(parameters);
            }
            Log.d(TAG, "Obtained camera instance");
            return Optional.fromNullable(camera);
        } catch (Exception e) {
            Log.w(TAG, "Exception occurred while obtaining camera. " + e.getMessage());
        }
        return Optional.absent();
    }
}