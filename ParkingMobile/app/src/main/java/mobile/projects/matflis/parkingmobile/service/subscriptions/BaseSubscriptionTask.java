package mobile.projects.matflis.parkingmobile.service.subscriptions;

import mobile.projects.matflis.parkingmobile.activity.BaseSubscriptionActivity;
import mobile.projects.matflis.parkingmobile.service.core.ProgressDialogTask;


abstract class BaseSubscriptionTask<U, V, W extends BaseSubscriptionActivity> extends ProgressDialogTask<U, V, W> {
    final SubscriptionRemoteHandler subscriptionRemoteHandler = SubscriptionRemoteHandler.getInstance();

    BaseSubscriptionTask(W activity) {
        super(activity);
    }
}