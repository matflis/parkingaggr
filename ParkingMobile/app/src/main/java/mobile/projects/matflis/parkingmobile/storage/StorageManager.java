package mobile.projects.matflis.parkingmobile.storage;

import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.common.base.Optional;
import com.google.common.io.Files;

import org.joda.time.DateTime;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

public final class StorageManager {

    private static final String TAG = "StorageManager";
    private static String APP_HOME_FOLDER;
    private static final String TMP_FOLDER_NAME = "tmp";
    private static final String SUBSCRIPTIONS_FOLDER_NAME = "subscriptions";

    static {
        File appHome = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "ParkingMobile");
        Log.d(TAG, "Initializing ParkingMobile storage folder");
        if (appHome.exists()) {
            Log.d(TAG, "App home exists. Skipping creation of app home");
        } else {
            Log.d(TAG, "App home does not exist. Creating home folder");
            if (appHome.mkdir()) {
                Log.d(TAG, "App home created. Proceeding...");
            } else {
                Log.e(TAG, "Cannot create app home. Exiting...");
                throw new IllegalStateException("Cannot create home folder: " + appHome.getName());
            }
        }
        APP_HOME_FOLDER = appHome.getAbsolutePath();

        // Create app folders (tmp & subscriptions)
        Log.d(TAG, "Initializing tmp folder");
        createFolder(appHome, TMP_FOLDER_NAME);
        Log.d(TAG, "Tmp folder initialized.");
        Log.d(TAG, "Initializing subscriptions folder");
        createFolder(appHome, SUBSCRIPTIONS_FOLDER_NAME);
        Log.d(TAG, "Subscriptions folder initialized.");
    }

    private static void createFolder(File appHome, String name) {
        final File folder = new File(appHome, name);
        if (folder.exists()) {
            Log.d(TAG, "Folder: " + name + " already exists. Skipping initialization");
        } else {
            if (folder.mkdir()) {
                Log.d(TAG, "Folder: " + name + " created successfully");
            } else {
                Log.e(TAG, "Cannot create folder: " + name + ". Exiting");
            }
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static String saveToTmp(byte[] data) throws IOException {
        File tmpFile = new File(getAppPath(TMP_FOLDER_NAME), UUID.randomUUID().toString());
        tmpFile.createNewFile();
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(tmpFile);
            fos.write(data);
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    // Nothing to do
                }
            }
        }
        return tmpFile.getAbsolutePath();
    }

    public static boolean moveTo(String source, String destination) {
        File sourceFile = new File(source);
        File destinationFile = new File(destination);
        try {
            Log.d(TAG, "Moving file: " + source + " to: " + destination);
            Files.move(sourceFile, destinationFile);
            Log.d(TAG, "File: " + source + " transferred successfully");
            return true;
        } catch (IOException e) {
            Log.e(TAG, "File: " + source + " was not moved successfully to: " + destination, e);
        }
        return false;
    }

    public static Optional<String> move(String oldLocation, String caseId) {
        File caseFolder = new File(getAppPath(SUBSCRIPTIONS_FOLDER_NAME), caseId);
        Log.d(TAG, "Creating folder for caseId: " + caseId);
        if (caseFolder.mkdir()) {
            Log.d(TAG, "Case folder: " + caseId + " created. Moving file from tmp location: " + oldLocation + " to a new one");
            File caseIdPictureFile = new File(caseFolder, DateTime.now().toString("yyyy-MM-dd HH:mm:SS") + ".jpg");
            File oldFile = new File(oldLocation);
            try {
                Files.move(oldFile, caseIdPictureFile);
                return Optional.of(caseIdPictureFile.getAbsolutePath());
            } catch (IOException e) {
                Log.e(TAG, "Exception while moving old file: " + oldFile.getPath() + " to a location: " + caseIdPictureFile.getPath(), e);
            }
        } else {
            Log.e(TAG, "Cannot create folder for case: " + caseId);
        }
        return Optional.absent();
    }

    public static String createTicketFile(String caseId) {
        File caseFolder = new File(getAppPath(SUBSCRIPTIONS_FOLDER_NAME), caseId);
        if (!caseFolder.exists()) {
            caseFolder.mkdir();
        }
        return new File(caseFolder, "ticket.pdf").getAbsolutePath();
    }

    @NonNull
    private static String getAppPath(String folder) {
        return APP_HOME_FOLDER + File.separator + folder;
    }

    private StorageManager() {
    }
}