package mobile.projects.matflis.parkingmobile.constants;

public final class Codes {

    @SuppressWarnings("unused")
    private Codes() {
    }

    public static final int LOCATION_REQUEST_CODE = 1_001;
    public static final int CAMERA_REQUEST_CODE = 1_002;
}