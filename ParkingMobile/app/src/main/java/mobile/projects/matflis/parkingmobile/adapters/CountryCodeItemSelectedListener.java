package mobile.projects.matflis.parkingmobile.adapters;


import pl.matflis.parkingweb.core.subscriptions.model.CountryCode;

public class CountryCodeItemSelectedListener extends SpinnerItemSelected<CountryCode> {

    @Override
    protected CountryCode parseValue(String value) {
        return CountryCode.valueOf(value);
    }
}