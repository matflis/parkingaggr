package mobile.projects.matflis.parkingmobile.service;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import com.google.common.base.Optional;

import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.List;

import pl.matflis.parkingweb.core.common.Position;

import static org.apache.commons.collections4.CollectionUtils.isEmpty;

public class StreetNameService {

    private final static String TAG = "StreetNameService";

    private final Context applicationContext;

    public StreetNameService(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    public Optional<String> getStreetName(Position position) throws IOException {
        Geocoder geocoder = new Geocoder(applicationContext);
        List<Address> addressList = geocoder.getFromLocation(position.getLatitude(), position.getLongitude(), 1);
        if (!isEmpty(addressList)) {
            return firstNonNull(addressList);
        }
        Log.d(TAG, "No addresses were found");
        return Optional.absent();
    }

    private Optional<String> firstNonNull(List<Address> addressList) {
        for (Address address : addressList) {
            String possibleAddress = address.getThoroughfare();
            if (StringUtils.isNotBlank(possibleAddress)) {
                Log.d(TAG, String.format("Potential address found '%s'", possibleAddress));
                return Optional.of(possibleAddress);
            }
        }
        Log.d(TAG, "No addresses were found");
        return Optional.absent();
    }
}