package mobile.projects.matflis.parkingmobile.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTimeZone;

import java.util.List;

import mobile.projects.matflis.parkingmobile.R;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionDTO;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionEntryInfo;

import static com.google.common.base.Preconditions.checkNotNull;


public class SubscriptionsAdapter extends BaseAdapter {

    private static final String OFFENCE_TIME_DISPLAY_FORMAT = "yyyy-MM-dd HH:mm";

    private final LayoutInflater inflater;
    private final List<SubscriptionEntryInfo> subscriptionEntries;

    public SubscriptionsAdapter(Context context, List<SubscriptionEntryInfo> subscriptionEntries) {
        this.inflater = LayoutInflater.from(checkNotNull(context, "Context cannot be null"));
        this.subscriptionEntries = checkNotNull(subscriptionEntries, "Subscription list has to be specified");
    }

    @Override
    public int getCount() {
        return subscriptionEntries.size();
    }

    @Override
    public Object getItem(int position) {
        return subscriptionEntries.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.transaction_item, null);
            holder = getHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        SubscriptionEntryInfo subscriptionItem = subscriptionEntries.get(position);
        SubscriptionDTO subscriptionDTO = subscriptionItem.getSubscriptionDTO();

        holder.mSubscriptionTime.setText(subscriptionItem.getOffenceTime().toDateTime(DateTimeZone.forOffsetHours(2))
                .toString(OFFENCE_TIME_DISPLAY_FORMAT));
        holder.mStreetName.setText(subscriptionDTO.getStreetName());
        holder.mCarDetails.setText(getCarDetails(subscriptionDTO));
        holder.mRemarks.setText(getRemarks(subscriptionDTO));
        holder.mViolation.setText(getViolationInfo(subscriptionDTO));

        Bitmap offencePicture = BitmapFactory.decodeFile(subscriptionItem.getFilePath());
        holder.mImageView.setImageBitmap(offencePicture);

        return convertView;
    }

    private ViewHolder getHolder(View convertView) {
        ViewHolder holder = new ViewHolder();
        holder.mSubscriptionTime = (TextView) convertView.findViewById(R.id.transactionDate);
        holder.mStreetName = (TextView) convertView.findViewById(R.id.transactionStreet);
        holder.mCarDetails = (TextView) convertView.findViewById(R.id.transactionCarDetails);
        holder.mRemarks = (TextView) convertView.findViewById(R.id.transactionRemarks);
        holder.mViolation = (TextView) convertView.findViewById(R.id.transactionViolation);
        holder.mImageView = (ImageView) convertView.findViewById(R.id.transactionImage);
        return holder;
    }

    @NonNull
    private String getCarDetails(SubscriptionDTO subscriptionDTO) {
        return subscriptionDTO.getBrand() + ", " + subscriptionDTO.getCountryCode().toString() + " " + subscriptionDTO.getRegistrationNumber();
    }

    private String getRemarks(SubscriptionDTO subscriptionDTO) {
        String remarks = subscriptionDTO.getRemarks();
        if (StringUtils.isNotBlank(remarks)) {
            return remarks;
        }
        return "Brak";
    }

    @NonNull
    private String getViolationInfo(SubscriptionDTO subscriptionDTO) {
        return subscriptionDTO.getViolationType().getDescription() + " - " + subscriptionDTO.getTicketAmount() + " zł";
    }

    private static class ViewHolder {
        TextView mSubscriptionTime;
        TextView mStreetName;
        TextView mCarDetails;
        TextView mRemarks;
        TextView mViolation;
        ImageView mImageView;
    }
}