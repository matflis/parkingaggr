package mobile.projects.matflis.parkingmobile.service.print;

import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionEntryInfo;

public interface PenaltyWriter {
    void print(SubscriptionEntryInfo subscription, String destinationPath) throws Exception;
}