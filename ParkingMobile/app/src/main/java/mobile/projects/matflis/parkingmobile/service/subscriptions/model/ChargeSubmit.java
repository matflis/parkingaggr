package mobile.projects.matflis.parkingmobile.service.subscriptions.model;

public class ChargeSubmit {

    private final String streetName;
    private final String registrationPlate;
    private final String brand;
    private final String remarks;
    private final String violation;
    private final String fine;
    private final String filePath;
    private final Long fineTime;

    public ChargeSubmit(String streetName, String registrationPlate, String brand, String remarks, String violation, String fine, String filePath, Long fineTime) {
        this.streetName = streetName;
        this.registrationPlate = registrationPlate;
        this.brand = brand;
        this.remarks = remarks;
        this.violation = violation;
        this.fine = fine;
        this.filePath = filePath;
        this.fineTime = fineTime;
    }

    public String getStreetName() {
        return streetName;
    }

    public String getRegistrationPlate() {
        return registrationPlate;
    }

    public String getBrand() {
        return brand;
    }

    public String getRemarks() {
        return remarks;
    }

    public String getViolation() {
        return violation;
    }

    public String getFine() {
        return fine;
    }

    public String getFilePath() {
        return filePath;
    }

    public Long getFineTime() {
        return fineTime;
    }
}