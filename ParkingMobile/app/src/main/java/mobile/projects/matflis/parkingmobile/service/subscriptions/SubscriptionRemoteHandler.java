package mobile.projects.matflis.parkingmobile.service.subscriptions;

import mobile.projects.matflis.parkingmobile.service.WebRemoteHandler;
import pl.matflis.parkingweb.core.subscriptions.model.ChargeResult;
import pl.matflis.parkingweb.core.subscriptions.model.CheckSubscriptionDto;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionInfo;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionSubmissionDTO;
import pl.matflis.parkingweb.core.util.SerDeMapper;

final class SubscriptionRemoteHandler {

    private static final String BASE_SUBSCRIPTIONS_ENDPOINT = "subscriptions";
    private static final String CHARGE_ENDPOINT = "charge";
    private static final String CHECK_ENDPOINT = "check";
    private static final String RECOGNISE_ENDPOINT = "recognise";

    private static final SubscriptionRemoteHandler Instance = new SubscriptionRemoteHandler();

    private final WebRemoteHandler webRemoteHandler = WebRemoteHandler.getInstance();

    static SubscriptionRemoteHandler getInstance() {
        return Instance;
    }

    private SubscriptionRemoteHandler() {
    }

    String getPlate(byte[] plate) throws Exception {
        return performRequest(RECOGNISE_ENDPOINT, plate);
    }

    SubscriptionInfo checkSubscription(CheckSubscriptionDto checkSubscriptionDto) throws Exception {
        String subscriptionResult = performRequest(CHECK_ENDPOINT, SerDeMapper.toJson(checkSubscriptionDto).getBytes());
        return SerDeMapper.fromJson(subscriptionResult, SubscriptionInfo.class);
    }

    ChargeResult charge(SubscriptionSubmissionDTO params) throws Exception {
        return SerDeMapper.fromJson(performRequest(CHARGE_ENDPOINT, SerDeMapper.toJson(params).getBytes()), ChargeResult.class);
    }

    @SuppressWarnings("unchecked")
    private String performRequest(String endpoint, byte[] postData) throws Exception {
        return webRemoteHandler.post(BASE_SUBSCRIPTIONS_ENDPOINT + "/" + endpoint, postData);
    }
}