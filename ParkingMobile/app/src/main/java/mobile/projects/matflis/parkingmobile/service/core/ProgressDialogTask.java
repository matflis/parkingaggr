package mobile.projects.matflis.parkingmobile.service.core;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import mobile.projects.matflis.parkingmobile.providers.ToastProvider;

public abstract class ProgressDialogTask<U, V, T extends Activity> extends AsyncTask<U, Void, TaskResult<V>> {

    private final ProgressDialog dialog;
    protected final T activity;

    public ProgressDialogTask(T activity) {
        this.activity = activity;
        this.dialog = new ProgressDialog(activity);
    }

    @Override
    protected void onPreExecute() {
        this.dialog.setMessage(getExecuteMessage());
        this.dialog.show();
    }

    public abstract String getExecuteMessage();

    @SuppressWarnings("unchecked")
    @Override
    protected TaskResult<V> doInBackground(U... params) {
        try {
            final V taskResult = doInBackgroundInternal(params[0]);
            return new TaskResult<>(taskResult);
        } catch (Exception e) {
            return new TaskResult<>(e);
        }
    }

    protected abstract V doInBackgroundInternal(U param) throws Exception;

    @Override
    protected void onPostExecute(TaskResult<V> result) {
        super.onPostExecute(result);
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    protected void showError(TaskResult<V> result) {
        ToastProvider.showLongToast(activity, result.getExceptionMessage());
    }
}