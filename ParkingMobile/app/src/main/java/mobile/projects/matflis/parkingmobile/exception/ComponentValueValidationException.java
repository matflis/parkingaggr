package mobile.projects.matflis.parkingmobile.exception;

public class ComponentValueValidationException extends ParkingMobileException {

    public ComponentValueValidationException(String message) {
        super(message);
    }
}