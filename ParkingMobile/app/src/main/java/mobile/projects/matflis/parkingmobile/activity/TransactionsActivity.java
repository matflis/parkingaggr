package mobile.projects.matflis.parkingmobile.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import java.io.File;
import java.util.List;

import mobile.projects.matflis.parkingmobile.R;
import mobile.projects.matflis.parkingmobile.adapters.SubscriptionsAdapter;
import mobile.projects.matflis.parkingmobile.providers.ToastProvider;
import mobile.projects.matflis.parkingmobile.service.print.PdfPenaltyPrinter;
import mobile.projects.matflis.parkingmobile.service.print.PenaltyWriter;
import mobile.projects.matflis.parkingmobile.service.subscriptions.SubscriptionService;
import mobile.projects.matflis.parkingmobile.storage.StorageManager;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionEntryInfo;

import static android.widget.AdapterView.AdapterContextMenuInfo;


public class TransactionsActivity extends AppCompatActivity {

    private static final String TAG = "TransactionsActivity";

    private SubscriptionsAdapter subscriptionsAdapter;
    private PenaltyWriter penaltyWriter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transactions_activity);
        setSupportActionBar((Toolbar) findViewById(R.id.transactionsToolbar));

        SubscriptionService subscriptionService = SubscriptionService.get(this);
        List<SubscriptionEntryInfo> subscriptions = subscriptionService.getSubscriptions();

        subscriptionsAdapter = new SubscriptionsAdapter(this, subscriptions);

        ListView subscriptionsList = (ListView) findViewById(R.id.transactionList);
        subscriptionsList.setAdapter(subscriptionsAdapter);
        registerForContextMenu(subscriptionsList);

        penaltyWriter = PdfPenaltyPrinter.getInstance();
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.transactionsmenu, menu);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        String printToPdfText = getString(R.string.transactionPrintPdf);
        menu.add(0, 1, 0, printToPdfText);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterContextMenuInfo menuInfo = (AdapterContextMenuInfo) item.getMenuInfo();
        if (item.getItemId() == 1) {
            SubscriptionEntryInfo subscriptionItem = (SubscriptionEntryInfo) subscriptionsAdapter.getItem((int) menuInfo.id);
            printContent(subscriptionItem);
            return true;
        } else {
            return super.onContextItemSelected(item);
        }
    }

    private void printContent(SubscriptionEntryInfo subscriptionEntry) {
        String destinationPath = StorageManager.createTicketFile(subscriptionEntry.getSubscriptionDTO().getUuid());
        try {
            penaltyWriter.print(subscriptionEntry, destinationPath);
            File file = new File(destinationPath);
            Intent target = new Intent(Intent.ACTION_VIEW);
            target.setDataAndType(Uri.fromFile(file), "application/pdf");
            target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

            Intent intent = Intent.createChooser(target, "Open File");
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                // Instruct the user to install a PDF reader here, or something
                ToastProvider.showLongToast(this, "Brak aplikacji pozwalajacej na otwieranie plikow PDF");
            }
        } catch (Exception e) {
            Log.e(TAG, "Cannot print ticket. Reason: " + e.getMessage(), e);
            ToastProvider.showLongToast(getApplicationContext(), "Błąd podczas wydruku mandatu.");
        }
    }
}