package mobile.projects.matflis.parkingmobile.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

import java.io.ByteArrayOutputStream;

public final class ImageUtils {

    private static final int SCALE = 10;

    public static byte[] compress(byte image[]) {
        Bitmap originalImage = BitmapFactory.decodeByteArray(image, 0, image.length);
        // Samsung hack as it rotates image by default
        // TODO: Extract exif data and rotate only if needed
        originalImage = rotateImage(originalImage, 90);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        originalImage.compress(Bitmap.CompressFormat.JPEG, SCALE, stream);
        return stream.toByteArray();
    }

    private static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    private ImageUtils() {
    }
}