package mobile.projects.matflis.parkingmobile.service.subscriptions;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.ArrayList;
import java.util.List;

import mobile.projects.matflis.parkingmobile.service.subscriptions.model.ChargeSubmit;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionDTO;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionEntryInfo;
import pl.matflis.parkingweb.core.subscriptions.model.ViolationType;


final class SubscriptionsDao {

    private final SubscriptionsDbHelper dbHelper;
    private static SubscriptionsDao Instance;

    static SubscriptionsDao get(Context context) {
        if (Instance == null) {
            Instance = new SubscriptionsDao(context);
        }
        return Instance;
    }

    private SubscriptionsDao(Context context) {
        context.deleteDatabase("Subscriptions.db");
        this.dbHelper = new SubscriptionsDbHelper(context);
    }

    void addCharge(ChargeSubmit chargeSubmit) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SubscriptionEntry.COLUMN_STREET_NAME, chargeSubmit.getStreetName());
        values.put(SubscriptionEntry.COLUMN_REGISTRATION_NUMBER, chargeSubmit.getRegistrationPlate());
        values.put(SubscriptionEntry.COLUMN_BRAND, chargeSubmit.getBrand());
        values.put(SubscriptionEntry.COLUMN_REMARKS, chargeSubmit.getRemarks());
        values.put(SubscriptionEntry.COLUMN_VIOLATION, chargeSubmit.getViolation());
        values.put(SubscriptionEntry.COLUMN_FINE, chargeSubmit.getFine());
        values.put(SubscriptionEntry.COLUMN_PATH, chargeSubmit.getFilePath());
        values.put(SubscriptionEntry.COLUMN_FINE_TIME, chargeSubmit.getFineTime());

        db.insert(SubscriptionEntry.TABLE_NAME, null, values);
    }

    List<SubscriptionEntryInfo> getSubscriptions() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String[] projection = {
                SubscriptionEntry.COLUMN_STREET_NAME,
                SubscriptionEntry.COLUMN_REGISTRATION_NUMBER,
                SubscriptionEntry.COLUMN_BRAND,
                SubscriptionEntry.COLUMN_REMARKS,
                SubscriptionEntry.COLUMN_VIOLATION,
                SubscriptionEntry.COLUMN_FINE,
                SubscriptionEntry.COLUMN_PATH,
                SubscriptionEntry.COLUMN_FINE_TIME
        };
        String sortOrder = SubscriptionEntry.COLUMN_FINE_TIME + " DESC";

        Cursor cursor = null;
        try {
            List<SubscriptionEntryInfo> subscriptions = new ArrayList<>();
            cursor = db.query(SubscriptionEntry.TABLE_NAME, projection, null, null, null, null,
                    sortOrder);
            while (cursor.moveToNext()) {
                SubscriptionDTO subscriptionDTO = new SubscriptionDTO.SubscriptionDTOBuilder()
                        .withStreetName(getValue(cursor, SubscriptionEntry.COLUMN_STREET_NAME))
                        .withRegistrationNumber(getValue(cursor, SubscriptionEntry.COLUMN_REGISTRATION_NUMBER))
                        .withBrand(getValue(cursor, SubscriptionEntry.COLUMN_BRAND))
                        .withRemarks(getValue(cursor, SubscriptionEntry.COLUMN_REMARKS))
                        .withViolationType(ViolationType.fromDescription(getValue(cursor, SubscriptionEntry.COLUMN_VIOLATION)))
                        .withTicketAmount(Integer.valueOf(getValue(cursor, SubscriptionEntry.COLUMN_FINE)))
                        .build();
                String path = getValue(cursor, SubscriptionEntry.COLUMN_PATH);
                String fineTime = getValue(cursor, SubscriptionEntry.COLUMN_FINE_TIME);
                DateTime offenceTime = new DateTime(Long.valueOf(fineTime), DateTimeZone.UTC);
                subscriptions.add(new SubscriptionEntryInfo(subscriptionDTO, path, offenceTime));
            }
            return subscriptions;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private String getValue(Cursor cursor, String columnName) {
        return cursor.getString(cursor.getColumnIndex(columnName));
    }

    static class SubscriptionEntry implements BaseColumns {
        static final String COLUMN_UUID = "uuid";
        static final String TABLE_NAME = "subscriptions";
        static final String COLUMN_STREET_NAME = "street";
        static final String COLUMN_REGISTRATION_NUMBER = "registration";
        static final String COLUMN_BRAND = "brand";
        static final String COLUMN_REMARKS = "remarks";
        static final String COLUMN_VIOLATION = "violation";
        static final String COLUMN_FINE = "fine";
        static final String COLUMN_PATH = "filepath";
        static final String COLUMN_FINE_TIME = "fine_time";
    }
}