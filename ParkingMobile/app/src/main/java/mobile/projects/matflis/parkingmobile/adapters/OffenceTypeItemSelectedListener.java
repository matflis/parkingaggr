package mobile.projects.matflis.parkingmobile.adapters;


import pl.matflis.parkingweb.core.subscriptions.model.ViolationType;

public class OffenceTypeItemSelectedListener extends SpinnerItemSelected<ViolationType> {

    @Override
    protected ViolationType parseValue(String value) {
        return ViolationType.fromDescription(value);
    }
}