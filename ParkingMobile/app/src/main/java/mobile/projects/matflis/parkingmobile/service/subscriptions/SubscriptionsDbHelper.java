package mobile.projects.matflis.parkingmobile.service.subscriptions;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

final class SubscriptionsDbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Subscriptions.db";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + SubscriptionsDao.SubscriptionEntry.TABLE_NAME + " (" +
                    SubscriptionsDao.SubscriptionEntry._ID + " INTEGER PRIMARY KEY," +
                    SubscriptionsDao.SubscriptionEntry.COLUMN_UUID + " TEXT," +
                    SubscriptionsDao.SubscriptionEntry.COLUMN_STREET_NAME + " TEXT," +
                    SubscriptionsDao.SubscriptionEntry.COLUMN_REGISTRATION_NUMBER + " TEXT," +
                    SubscriptionsDao.SubscriptionEntry.COLUMN_BRAND + " TEXT," +
                    SubscriptionsDao.SubscriptionEntry.COLUMN_REMARKS + " TEXT," +
                    SubscriptionsDao.SubscriptionEntry.COLUMN_VIOLATION + " TEXT," +
                    SubscriptionsDao.SubscriptionEntry.COLUMN_FINE + " TEXT," +
                    SubscriptionsDao.SubscriptionEntry.COLUMN_PATH + " TEXT," +
                    SubscriptionsDao.SubscriptionEntry.COLUMN_FINE_TIME + " INTEGER)";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + SubscriptionsDao.SubscriptionEntry.TABLE_NAME;

    SubscriptionsDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
}