package mobile.projects.matflis.parkingmobile.service.core;

import static com.google.common.base.Preconditions.checkArgument;

public class TaskResult<T> {

    private final T result;
    private final Exception exception;

    public TaskResult(T result) {
        this(result, null);
    }

    public TaskResult(Exception e) {
        this(null, e);
    }

    private TaskResult(T result, Exception exception) {
        checkArgument(result == null && exception != null || result != null &&
                exception == null, "Only one of the arguments can be null");
        this.result = result;
        this.exception = exception;
    }

    public boolean isSuccessful() {
        return exception == null;
    }

    public boolean hasFailed() {
        return !isSuccessful();
    }

    public T getResult() {
        if (result == null) {
            throw new IllegalStateException("Getting result while it's empty");
        }
        return result;
    }

    public String getExceptionMessage() {
        if (exception == null) {
            throw new IllegalStateException("Getting exception which is empty");
        }
        return exception.getMessage();
    }
}