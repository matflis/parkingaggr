package mobile.projects.matflis.parkingmobile.exception;

public class RemotePostException extends RuntimeException {

    public RemotePostException(String message) {
        super(message);
    }
}