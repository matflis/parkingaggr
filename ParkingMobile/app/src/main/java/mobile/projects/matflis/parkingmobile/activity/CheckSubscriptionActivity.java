package mobile.projects.matflis.parkingmobile.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import mobile.projects.matflis.parkingmobile.R;

public class CheckSubscriptionActivity extends BaseSubscriptionActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.subscription_check_activity);

        setLayoutInitials();
        View pictureButton = findViewById(R.id.takeRegistrationPlatePictureButton);
        pictureButton.setVisibility(View.GONE);
    }
}