package mobile.projects.matflis.parkingmobile.service.subscriptions;

import mobile.projects.matflis.parkingmobile.activity.BaseSubscriptionActivity;
import mobile.projects.matflis.parkingmobile.service.core.TaskResult;
import pl.matflis.parkingweb.core.subscriptions.model.CheckSubscriptionDto;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionInfo;

public class CheckSubscriptionTask extends BaseSubscriptionTask<CheckSubscriptionDto, SubscriptionInfo, BaseSubscriptionActivity> {

    public CheckSubscriptionTask(BaseSubscriptionActivity activity) {
        super(activity);
    }

    @Override
    public String getExecuteMessage() {
        return "Sprawdzam bilet";
    }

    @Override
    protected SubscriptionInfo doInBackgroundInternal(CheckSubscriptionDto param) throws Exception {
        return subscriptionRemoteHandler.checkSubscription(param);
    }

    @Override
    protected void onPostExecute(TaskResult<SubscriptionInfo> result) {
        super.onPostExecute(result);
        if (result.isSuccessful()) {
            activity.showPopup(result.getResult().toString());
        } else {
            showError(result);
        }
    }
}