package mobile.projects.matflis.parkingmobile.service.subscriptions.exception;

public final class SubscriptionPostException extends RuntimeException {

    public SubscriptionPostException(String message, Exception e) {
        super(message, e);
    }
}