package mobile.projects.matflis.parkingmobile.exception;

public abstract class ParkingMobileException extends RuntimeException {
    public ParkingMobileException(String message) {
        super(message);
    }

    ParkingMobileException(String message, Throwable cause) {
        super(message, cause);
    }
}