package mobile.projects.matflis.parkingmobile.activity;

import android.Manifest;
import android.app.DialogFragment;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.common.base.Function;
import com.google.common.base.Optional;

import mobile.projects.matflis.parkingmobile.R;
import mobile.projects.matflis.parkingmobile.adapters.BrandNameItemSelectedListener;
import mobile.projects.matflis.parkingmobile.adapters.CarTypeItemSelectedListener;
import mobile.projects.matflis.parkingmobile.adapters.OffenceTypeItemSelectedListener;
import mobile.projects.matflis.parkingmobile.adapters.SpinnerItemSelected;
import mobile.projects.matflis.parkingmobile.components.InterruptDialog;
import mobile.projects.matflis.parkingmobile.config.ParameterManager;
import mobile.projects.matflis.parkingmobile.config.ParameterManager.Parameter;
import mobile.projects.matflis.parkingmobile.constants.Codes;
import mobile.projects.matflis.parkingmobile.exception.ParkingMobileException;
import mobile.projects.matflis.parkingmobile.exception.SubscriptionSubmissionException;
import mobile.projects.matflis.parkingmobile.providers.CameraProvider;
import mobile.projects.matflis.parkingmobile.providers.ToastProvider;
import mobile.projects.matflis.parkingmobile.service.subscriptions.ChargeTask;
import mobile.projects.matflis.parkingmobile.service.subscriptions.GetPlateNumberTableTask;
import mobile.projects.matflis.parkingmobile.service.subscriptions.exception.SubscriptionFileSaveException;
import mobile.projects.matflis.parkingmobile.storage.StorageManager;
import mobile.projects.matflis.parkingmobile.utils.ImageUtils;
import mobile.projects.matflis.parkingmobile.utils.PermissionProviderUtil;
import mobile.projects.matflis.parkingmobile.views.CameraPreview;
import pl.matflis.parkingweb.core.common.Position;
import pl.matflis.parkingweb.core.subscriptions.model.CountryCode;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionDTO;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionEntryInfo;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionSubmissionDTO;
import pl.matflis.parkingweb.core.subscriptions.model.ViolationType;

import static pl.matflis.parkingweb.core.subscriptions.model.SubscriptionDTO.SubscriptionDTOBuilder;

public class ChargeActivity extends BaseSubscriptionActivity {

    private final static String TAG = "ChargeActivity";

    // Spinner listeners
    private final SpinnerItemSelected<String> brandNameItemListener = new BrandNameItemSelectedListener();
    private final SpinnerItemSelected<String> carTypeItemListener = new CarTypeItemSelectedListener();
    private final SpinnerItemSelected<ViolationType> offenceTypeItemListener = new OffenceTypeItemSelectedListener();

    private Optional<Camera> cameraOptional;
    private String filePath = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.charge_activity);

        setLayoutInitials();

        cameraOptional = CameraProvider.get();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (cameraOptional != null && cameraOptional.isPresent()) {
            cameraOptional.get().release();
        }
    }

    @Override
    void setLayoutInitials() {
        super.setLayoutInitials();

        initializeSpinner(R.id.brandNameSpinner, R.array.brand_names, brandNameItemListener);
        initializeSpinner(R.id.carTypeSpinner, R.array.car_types, carTypeItemListener);

        initializeSpinner(R.id.offenceTypeSpinner, R.array.offence_types, offenceTypeItemListener);
        offenceTypeItemListener.addListener(new Function<ViolationType, Void>() {
            @Override
            public Void apply(ViolationType input) {
                EditText offenceText = (EditText) findViewById(R.id.offenceAmountText);
                offenceText.setText(input.getAmountAsString());
                return null;
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == Codes.CAMERA_REQUEST_CODE) {
            if (grantResults.length > 0) {
                // This means that user has granted permissions to camera
                takePicture();
            }
        }
    }


    public void onExitButtonClicked(View view) {
        DialogFragment dialogFragment = new InterruptDialog();
        dialogFragment.show(getFragmentManager(), "ExitFragment");
    }

    public void onSubmitPenaltyButtonClicked(View view) {
        try {
            String streetTextValue = validateStreetNameBlankness();
            String registrationValue = validateRegistrationPlateBlankness();

            Optional<CountryCode> countryCodeOptional = countryCodeItemListener.getValue();
            Optional<String> brandOptional = brandNameItemListener.getValue();
            Optional<String> carTypeOptional = carTypeItemListener.getValue();
            Optional<ViolationType> violationTypeOptional = offenceTypeItemListener.getValue();

            if (!countryCodeOptional.isPresent() || !brandOptional.isPresent() ||
                    !carTypeOptional.isPresent() || !violationTypeOptional.isPresent()) {
                return;
            }

            String remarks = ((TextView) findViewById(R.id.remarksTextView)).getText().toString();
            int offenceValue = Integer.valueOf(((TextView) findViewById(R.id.offenceAmountText)).getText().toString());

            SubscriptionDTO subscriptionDTO = new SubscriptionDTOBuilder()
                    .withStreetName(streetTextValue)
                    .withRegistrationNumber(registrationValue)
                    .withCountryCode(countryCodeOptional.get())
                    .withBrand(brandOptional.get())
                    .withRemarks(remarks)
                    .withViolationType(violationTypeOptional.get())
                    .withTicketAmount(offenceValue)
                    .build();
            String userId = ParameterManager.getParameterValue(Parameter.USERNAME);
            Optional<Position> positionOptional = locationService.getPositionOptional();
            if (!positionOptional.isPresent()) {
                throw new SubscriptionSubmissionException("Cannot submit charge. Position was not downloaded");
            }
            if (filePath == null) {
                throw new SubscriptionFileSaveException("Picture path was not taken");
            }
            SubscriptionEntryInfo subscriptionEntryInfo = new SubscriptionEntryInfo(subscriptionDTO, filePath);
            SubscriptionSubmissionDTO submissionDTO = new SubscriptionSubmissionDTO(userId, subscriptionEntryInfo, positionOptional.get());
            ChargeTask chargeTask = new ChargeTask(this);
            chargeTask.execute(submissionDTO);
        } catch (ParkingMobileException e) {
            Log.w(TAG, "Cannot save subscription. Reason: " + e.getMessage());
            ToastProvider.showLongToast(getApplicationContext(), e.getMessage());
        }
    }

    public void setPlateText(String text) {
        TextView registrationPlateText = (TextView) findViewById(R.id.registrationPlateText);
        registrationPlateText.setText(text);
    }

    public void onTakeRegistrationPlatePictureButtonClicked(View view) {
        boolean cameraGranted = PermissionProviderUtil.isAllowed(this, Manifest.permission.CAMERA);
        boolean storageGranted = PermissionProviderUtil.isAllowed(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (!cameraGranted || !storageGranted) {
            String[] permissions = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
            ActivityCompat.requestPermissions(this, permissions, Codes.CAMERA_REQUEST_CODE);
        } else {
            if (getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
                // This device has a camera
                Log.d(TAG, "Camera exists on this phone. Picture may be taken");
                takePicture();
            } else {
                // No camera on this device
                Log.d(TAG, "Camera is not supported at this device. Button for taking pictures will be hidden");
                ToastProvider.showShortToast(getApplicationContext(), "Unfortunately, this device does not have camera");
            }
        }
    }

    private void takePicture() {
        if (cameraOptional.isPresent()) {
            final Camera camera = cameraOptional.get();
            Button captureButton = (Button) findViewById(R.id.takePictureButton);
            captureButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    camera.takePicture(null, null, new ChargeActivity.PictureCallbackImpl());
                }
            });

            FrameLayout cameraPreview = (FrameLayout) findViewById(R.id.camera_preview);
            LinearLayout cameraLayout = (LinearLayout) findViewById(R.id.camera_layout);
            LinearLayout chargeLayout = (LinearLayout) findViewById(R.id.charge_layout);
            chargeLayout.setVisibility(View.GONE);
            cameraPreview.addView(new CameraPreview(getApplicationContext(), camera));
            cameraLayout.setVisibility(View.VISIBLE);
        } else {
            ToastProvider.showShortToast(getApplicationContext(), "An error occurred while accessing camera. Photo will not be taken.");
        }
    }

    private class PictureCallbackImpl implements Camera.PictureCallback {

        private final static String TAG = "PictureCallbackImpl";

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            try {
                byte[] compressedImage = ImageUtils.compress(data);
                GetPlateNumberTableTask task = new GetPlateNumberTableTask(ChargeActivity.this);
                task.execute(compressedImage);
                filePath = StorageManager.saveToTmp(compressedImage);

                LinearLayout cameraLayout = (LinearLayout) findViewById(R.id.camera_layout);
                LinearLayout chargeLayout = (LinearLayout) findViewById(R.id.charge_layout);
                chargeLayout.setVisibility(View.VISIBLE);
                cameraLayout.setVisibility(View.GONE);
            } catch (Exception e) {
                String message = "Error while getting registration number: " + e.getMessage();
                Log.e(TAG, message, e);
                ToastProvider.showLongToast(getApplicationContext(), message);
            }
        }
    }
}