package mobile.projects.matflis.parkingmobile.constants;

public final class ConfigConstants {

    @SuppressWarnings("unused")
    private ConfigConstants() {
    }

    public static final int REFRESH_STREET_INTERVAL_DIALOG_SECONDS = 5;
}