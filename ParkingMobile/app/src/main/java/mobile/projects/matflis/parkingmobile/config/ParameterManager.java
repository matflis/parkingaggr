package mobile.projects.matflis.parkingmobile.config;

import java.util.EnumMap;
import java.util.Map;

public final class ParameterManager {

    private static final Map<Parameter, String> config = new EnumMap<>(Parameter.class);

    static {
        config.put(Parameter.SERVER_URL, "https://192.168.8.104:5050/");
        config.put(Parameter.CERTIFICATE_DIGEST, "47adb03649a2eb18f63ffa29790818349a99cab7");
    }

    public static String getParameterValue(Parameter parameter) {
        return config.get(parameter);
    }

    public static void setParameterValue(Parameter parameter, String value) {
        config.put(parameter, value);
    }

    private ParameterManager() {
    }

    public enum Parameter {
        SERVER_URL, CERTIFICATE_DIGEST, USERNAME
    }
}