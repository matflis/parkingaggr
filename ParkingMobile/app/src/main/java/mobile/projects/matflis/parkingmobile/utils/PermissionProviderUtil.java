package mobile.projects.matflis.parkingmobile.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;

public final class PermissionProviderUtil {

    public static boolean isAllowed(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    private PermissionProviderUtil() {
    }
}