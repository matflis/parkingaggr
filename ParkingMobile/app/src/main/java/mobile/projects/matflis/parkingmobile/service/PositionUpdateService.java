package mobile.projects.matflis.parkingmobile.service;

import android.util.Log;

import com.google.common.base.Optional;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import pl.matflis.parkingweb.core.common.Position;
import pl.matflis.parkingweb.core.position.PositionReadDTO;
import pl.matflis.parkingweb.core.util.SerDeMapper;

public class PositionUpdateService {

    private static ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
    private static boolean isInitialized = false;

    public static void start(LocationService locationService, String username) {
        if (!isInitialized) {
            executorService.scheduleWithFixedDelay(new PositionUpdateRunnable(locationService, username), 1, 1, TimeUnit.MINUTES);
            isInitialized = true;
        }
    }

    private static class PositionUpdateRunnable implements Runnable {

        private static final String TAG = "PositionUpdateRunnable";
        private static final String ENDPOINT = "positions";
        private final WebRemoteHandler webRemoteHandler = WebRemoteHandler.getInstance();
        private final LocationService locationService;
        private final String username;

        PositionUpdateRunnable(LocationService locationService, String username) {
            this.locationService = locationService;
            this.username = username;
        }

        @Override
        public void run() {
            Optional<Position> positionOptional = locationService.getPositionOptional();
            try {
                if (!positionOptional.isPresent()) {
                    throw new IllegalStateException("Position was not updated");
                }
                PositionReadDTO positionReadDTO = new PositionReadDTO(positionOptional.get(), username);
                String result = webRemoteHandler.post(ENDPOINT, SerDeMapper.toJson(positionReadDTO).getBytes());
                Log.d(TAG, result);
            } catch (Exception e) {
                Log.e(TAG, "Cannot send update position. Reason: " + e.getMessage(), e);
            }
        }
    }
}