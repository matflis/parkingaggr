package mobile.projects.matflis.parkingmobile.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import org.apache.commons.lang.StringUtils;

import java.util.Arrays;

import mobile.projects.matflis.parkingmobile.R;
import mobile.projects.matflis.parkingmobile.adapters.CountryCodeItemSelectedListener;
import mobile.projects.matflis.parkingmobile.adapters.SpinnerItemSelected;
import mobile.projects.matflis.parkingmobile.constants.Codes;
import mobile.projects.matflis.parkingmobile.exception.ComponentValueValidationException;
import mobile.projects.matflis.parkingmobile.handlers.StreetNameButtonHandler;
import mobile.projects.matflis.parkingmobile.service.LocationService;
import mobile.projects.matflis.parkingmobile.service.subscriptions.CheckSubscriptionTask;
import pl.matflis.parkingweb.core.subscriptions.model.CheckSubscriptionDto;
import pl.matflis.parkingweb.core.subscriptions.model.CountryCode;


public abstract class BaseSubscriptionActivity extends Activity {

    private static final String TAG = "BsSubscriptionActivity";

    final LocationService locationService = LocationService.get();

    final StreetNameButtonHandler streetNameButtonHandler = new StreetNameButtonHandler(BaseSubscriptionActivity.this);

    final SpinnerItemSelected<CountryCode> countryCodeItemListener = new CountryCodeItemSelectedListener();
    private LocationManager locationManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initLocationManager();
    }

    void setLayoutInitials() {
        initializeSpinner(R.id.registrationCodeCountrySpinner, R.array.registration_countries, countryCodeItemListener);
    }

    void initializeSpinner(int spinnerId, int arrayResourceId, SpinnerItemSelected<?> itemSelectedListener) {
        final Spinner spinner = (Spinner) findViewById(spinnerId);
        spinner.setAdapter(getCharSequenceArrayAdapter(arrayResourceId));
        spinner.setOnItemSelectedListener(itemSelectedListener);
    }


    @NonNull
    ArrayAdapter<CharSequence> getCharSequenceArrayAdapter(int itemsId) {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, itemsId, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return adapter;
    }

    private void initLocationManager() {
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, Codes.LOCATION_REQUEST_CODE);
                Log.d(TAG, "Requested for location permissions");
            } else {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    addLocationUpdates(LocationManager.GPS_PROVIDER);
                }
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    addLocationUpdates(LocationManager.NETWORK_PROVIDER);
                }
            }
        } else {
            addLocationUpdates(LocationManager.GPS_PROVIDER, LocationManager.NETWORK_PROVIDER);
            Log.d(TAG, "Build version < 24 - setting location updates");
        }
    }

    private void addLocationUpdates(String... providers) {
        for (String provider : providers) {
            locationManager.requestLocationUpdates(provider, 1, 0, locationService);
            Log.d(TAG, String.format("Launching %s Location Provider", provider));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == Codes.LOCATION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                int permittedLocations = 0;
                if (hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION, permissions, grantResults)) {
                    addLocationUpdates(LocationManager.NETWORK_PROVIDER);
                    permittedLocations++;
                }
                if (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION, permissions, grantResults)) {
                    addLocationUpdates(LocationManager.GPS_PROVIDER);
                    permittedLocations++;
                }
                if (permittedLocations == 0) {
                    disableLocationButton();
                }
            } else {
                // Disable button that allows to get user location
                disableLocationButton();
            }
        }
    }

    private boolean hasPermission(String permissionName, String[] permissions, int[] grantResults) {
        if (grantResults.length > 0) {
            final int permissionIndex = Arrays.asList(permissions).indexOf(permissionName);
            return grantResults[permissionIndex] == PackageManager.PERMISSION_GRANTED;
        }
        return false;
    }

    private void disableLocationButton() {
        final ImageButton streetLocationButton = (ImageButton) findViewById(R.id.locationButton);
        Log.d(TAG, "Disabling StreetDownload button.");
        streetLocationButton.setClickable(false);
    }

    // Handlers
    public void onStreetLocationButtonClicked(View view) {
        streetNameButtonHandler.performAction((TextView) findViewById(R.id.streetText));
    }

    public void onCheckSubscriptionClicked(View view) {
        try {
            final String streetName = validateStreetNameBlankness();
            final String registrationValue = validateRegistrationPlateBlankness();

            CheckSubscriptionTask checkSubscriptionTask = new CheckSubscriptionTask(this);
            checkSubscriptionTask.execute(new CheckSubscriptionDto(registrationValue, streetName));
        } catch (ComponentValueValidationException e) {
            Log.w(TAG, "Invalid field value: " + e.getMessage());
        }
    }

    @NonNull
    String validateRegistrationPlateBlankness() {
        return validateTextViewBlankness(R.id.registrationPlateText, "Registration plate cannot be empty");
    }

    @NonNull
    String validateStreetNameBlankness() {
        return validateTextViewBlankness(R.id.streetText, "Street name cannot be empty");
    }

    String validateTextViewBlankness(int viewId, String errorMsg) {
        final TextView textView = ((TextView) findViewById(viewId));
        final String textViewValue = textView.getText().toString();
        if (StringUtils.isBlank(textViewValue)) {
            textView.setError(errorMsg);
            Log.i(TAG, "Blank value for: " + textView.toString());
            throw new ComponentValueValidationException("Blank value for: " + textView.toString());
        }
        return textViewValue;
    }

    public void showPopup(String text) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setMessage(text)
                .setPositiveButton("OK", null)
                .create();
        dialog.show();
    }
}