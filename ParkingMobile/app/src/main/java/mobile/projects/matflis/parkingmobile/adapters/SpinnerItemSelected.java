package mobile.projects.matflis.parkingmobile.adapters;

import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.google.common.base.Function;
import com.google.common.base.Optional;

import java.util.ArrayList;
import java.util.List;

import mobile.projects.matflis.parkingmobile.R;

import static android.widget.AdapterView.OnItemSelectedListener;

public abstract class SpinnerItemSelected<T> implements OnItemSelectedListener {

    private T object;
    // Guava does not have 'Consumer' interface implemented
    private final List<Function<T, Void>> listeners = new ArrayList<>();

    public Optional<T> getValue() {
        return Optional.fromNullable(object);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // Hack for accessing resources from Spinner handler
        int textColor = parent.getResources().getColor(R.color.text, null);
        ((TextView) view).setTextColor(textColor);

        object = parseValue(parent.getItemAtPosition(position).toString());
        for (Function<T, Void> listener : listeners) {
            listener.apply(object);
        }
    }

    protected abstract T parseValue(String s);

    public void addListener(Function<T, Void> listener) {
        listeners.add(listener);
    }
}