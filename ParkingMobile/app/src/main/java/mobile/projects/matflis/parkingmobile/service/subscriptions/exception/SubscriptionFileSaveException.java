package mobile.projects.matflis.parkingmobile.service.subscriptions.exception;

import mobile.projects.matflis.parkingmobile.exception.ParkingMobileException;

public final class SubscriptionFileSaveException extends ParkingMobileException {

    public SubscriptionFileSaveException(String message) {
        super(message);
    }
}