package mobile.projects.matflis.parkingmobile.service.subscriptions;

import mobile.projects.matflis.parkingmobile.activity.ChargeActivity;
import mobile.projects.matflis.parkingmobile.service.core.TaskResult;

public class GetPlateNumberTableTask extends BaseSubscriptionTask<byte[], String, ChargeActivity> {

    public GetPlateNumberTableTask(ChargeActivity activity) {
        super(activity);
    }

    @Override
    public String getExecuteMessage() {
        return "Pobieram tablicę";
    }

    @Override
    protected String doInBackgroundInternal(byte[] param) throws Exception {
        return subscriptionRemoteHandler.getPlate(param);
    }

    @Override
    protected void onPostExecute(TaskResult<String> result) {
        super.onPostExecute(result);
        if (result.isSuccessful()) {
            activity.setPlateText(result.getResult());
        } else {
            showError(result);
        }
    }
}