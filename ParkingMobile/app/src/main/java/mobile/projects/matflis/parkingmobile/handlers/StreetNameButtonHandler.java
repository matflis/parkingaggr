package mobile.projects.matflis.parkingmobile.handlers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.TextView;

import com.google.common.base.Optional;

import java.io.IOException;

import mobile.projects.matflis.parkingmobile.R;
import mobile.projects.matflis.parkingmobile.constants.ConfigConstants;
import mobile.projects.matflis.parkingmobile.providers.ToastProvider;
import mobile.projects.matflis.parkingmobile.service.LocationService;
import mobile.projects.matflis.parkingmobile.service.StreetNameService;
import pl.matflis.parkingweb.core.common.Position;

import static java.lang.String.format;

public class StreetNameButtonHandler {

    private static final String TAG = "StreetNameButtonHandler";

    private final LocationService locationService;
    private final StreetNameService streetNameService;
    private final Context applicationContext;

    public StreetNameButtonHandler(Context applicationContext) {
        this.locationService = LocationService.get();
        this.streetNameService = new StreetNameService(applicationContext);
        this.applicationContext = applicationContext;
    }

    public void performAction(TextView streetTextView) {
        final Optional<Position> positionOptional = locationService.getPositionOptional();
        if (positionOptional.isPresent()) {
            updateStreetName(positionOptional.get(), streetTextView);
        } else {
            showMessage("Position hasn't been downloaded. Enable location or wait a second" +
                    " and allow app to get it.");
        }
    }

    private void updateStreetName(final Position position, final TextView streetTextView) {
        if ((System.currentTimeMillis() - position.getReadTime()) > ConfigConstants.REFRESH_STREET_INTERVAL_DIALOG_SECONDS * 1000) {
            AlertDialog.Builder builder = new AlertDialog.Builder(applicationContext);
            builder.setTitle(R.string.update_street_title)
                    .setMessage(R.string.update_street_message)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            applyStreetNameFind(position, streetTextView);
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {// Empty handler
                        }
                    });
            builder.create().show();
        } else {
            applyStreetNameFind(position, streetTextView);
        }
    }

    private void applyStreetNameFind(Position position, TextView streetTextView) {
        try {
            Optional<String> addressOptional = streetNameService.getStreetName(position);
            if (addressOptional.isPresent()) {
                streetTextView.setText(addressOptional.get());
                Log.i(TAG, format("Street '%s' was found for %s", addressOptional.get(), position));
            } else {
                showMessage("No reasonable location found for your coordinates");
                Log.i(TAG, format("No location found for '%s'", position));
            }
        } catch (IOException e) {
            Log.w(TAG, e.getMessage());
            showMessage(e.getMessage());
        }
    }

    private void showMessage(String message) {
        ToastProvider.showShortToast(applicationContext, message);
    }
}