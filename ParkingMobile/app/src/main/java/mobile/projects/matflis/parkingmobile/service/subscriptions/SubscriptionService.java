package mobile.projects.matflis.parkingmobile.service.subscriptions;

import android.content.Context;

import java.util.List;

import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionEntryInfo;


public class SubscriptionService {

    private final SubscriptionsDao subscriptionsDao;
    private static SubscriptionService subscriptionService;

    private SubscriptionService(Context context) {
        this.subscriptionsDao = SubscriptionsDao.get(context);
    }

    public static SubscriptionService get(Context context) {
        if (subscriptionService == null) {
            subscriptionService = new SubscriptionService(context);
        }
        return subscriptionService;
    }

    public List<SubscriptionEntryInfo> getSubscriptions() {
        return subscriptionsDao.getSubscriptions();
    }
}