CREATE TABLE IF NOT EXISTS Users (
  ID       BIGINT PRIMARY KEY AUTO_INCREMENT,
  Username VARCHAR(100) NOT NULL UNIQUE,
  Password VARCHAR(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS Charges (
  ID                BIGINT PRIMARY KEY AUTO_INCREMENT,
  UserID            BIGINT       NOT NULL,
  Longitude         DOUBLE       NOT NULL,
  Latitude          DOUBLE       NOT NULL,
  OffenceTimestamp  BIGINT       NOT NULL,
  StreetName        VARCHAR(100) NOT NULL,
  RegistrationPlate VARCHAR(100) NOT NULL,
  CountryCode       VARCHAR(10)  NOT NULL,
  Brand             VARCHAR(100) NOT NULL,
  Remarks           VARCHAR(100),
  ViolationType     VARCHAR(100) NOT NULL,
  TicketAmount      INT          NOT NULL,
  FOREIGN KEY (UserID) REFERENCES Users (ID),
  CHECK TicketAmount > 0,
  CHECK OffenceTimestamp > 0
);

MERGE INTO Users
KEY (ID) VALUES (1, 'admin', '');
MERGE INTO Users
KEY (ID) VALUES (2, 'test.user', '');