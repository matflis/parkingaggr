package pl.matflis.parking.viewer.server.db;

import org.joda.time.DateTime;
import org.sql2o.Connection;
import org.sql2o.ResultSetHandler;
import org.sql2o.Sql2o;
import pl.matflis.parkingweb.core.common.Position;
import pl.matflis.parkingweb.core.subscriptions.model.ChargeInfo;
import pl.matflis.parkingweb.core.subscriptions.model.SubscriptionDTO;
import pl.matflis.parkingweb.core.subscriptions.model.ViolationType;

import java.util.List;

public final class ChargeDao {

	private static final ChargeDao Instance = new ChargeDao();

	private final Sql2o sql2o;

	private ChargeDao() {
		this.sql2o = DbProvider.getDbConnector();
	}

	public static ChargeDao getInstance() {
		return Instance;
	}

	public List<ChargeInfo> getCharges() {
		final String query = "SELECT Username, StreetName, Brand, Longitude, Latitude, OffenceTimestamp, " +
				"RegistrationPlate, Remarks, ViolationType, TicketAmount " +
				"FROM Charges INNER JOIN Users on Charges.UserID = Users.ID";
		try (Connection connection = sql2o.open()) {
			return connection.createQuery(query)
					.executeAndFetch((ResultSetHandler<ChargeInfo>) resultSet -> {
						SubscriptionDTO subscriptionDTO = new SubscriptionDTO.SubscriptionDTOBuilder()
								.withTicketAmount(resultSet.getInt("TicketAmount"))
								.withViolationType(ViolationType.valueOf(resultSet.getString("ViolationType")))
								.withBrand(resultSet.getString("Brand"))
								.withRegistrationNumber(resultSet.getString("RegistrationPlate"))
								.withRemarks(resultSet.getString("Remarks"))
								.withStreetName(resultSet.getString("StreetName"))
								.build();
						Position position = new Position(resultSet.getDouble("Longitude"), resultSet.getDouble("Latitude"));
						DateTime offenceTimestamp = new DateTime(resultSet.getLong("OffenceTimestamp"));
						String username = resultSet.getString("Username");
						return new ChargeInfo(subscriptionDTO, position, offenceTimestamp, username);
					});
		}
	}
}