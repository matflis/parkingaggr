package pl.matflis.parking.viewer.server.db;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sql2o.Sql2o;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;

final class DbProvider {

	private static final Logger logger = LoggerFactory.getLogger(DbProvider.class);
	private static Sql2o sql2o;

	private DbProvider() {
		throw new RuntimeException("Violation exception in creating class");
	}

	static Sql2o getDbConnector() {
		if (sql2o == null) {
			logger.info("Initializing db connection");
			try {
				sql2o = new Sql2o(getDataSource());
				logger.debug("Db connection initialized");
			} catch (PropertyVetoException e) {
				logger.error("Cannot initialize connection. Reason: " + e.getMessage(), e);
				throw new DbConnectionInitializationException("Db connection initialization failed");
			}
		}
		return sql2o;
	}

	private static DataSource getDataSource() throws PropertyVetoException {
		ComboPooledDataSource dataSource = new ComboPooledDataSource();
		dataSource.setDriverClass(PropertiesSource.getValue(PropertiesSource.Property.JDBC_DRIVER_CLASS));
		dataSource.setJdbcUrl(PropertiesSource.getValue(PropertiesSource.Property.JDBC_URL));
		dataSource.setUser(PropertiesSource.getValue(PropertiesSource.Property.JDBC_USER));
		dataSource.setPassword(PropertiesSource.getValue(PropertiesSource.Property.JDBC_PASSWORD));
		return dataSource;
	}

	private static class DbConnectionInitializationException extends RuntimeException {
		DbConnectionInitializationException(String message) {
			super(message);
		}
	}
}