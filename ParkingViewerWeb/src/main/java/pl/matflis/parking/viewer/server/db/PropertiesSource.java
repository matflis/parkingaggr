package pl.matflis.parking.viewer.server.db;

import java.util.EnumMap;
import java.util.Map;

final class PropertiesSource {

	private static final Map<Property, String> propertiesValue = new EnumMap<>(Property.class);

	static {
		// TODO: Replace this with property reading
		propertiesValue.put(Property.JDBC_DRIVER_CLASS, "com.mysql.jdbc.Driver");
		propertiesValue.put(Property.JDBC_URL, "jdbc:mysql://localhost:3306/parkingviewerweb?serverTimezone=UTC");
		propertiesValue.put(Property.JDBC_USER, "root");
		propertiesValue.put(Property.JDBC_PASSWORD, "");
	}

	private PropertiesSource() {
	}

	static String getValue(Property property) {
		return propertiesValue.get(property);
	}

	public enum Property {
		JDBC_DRIVER_CLASS, JDBC_URL, JDBC_USER, JDBC_PASSWORD
	}
}