package pl.matflis.parking.viewer.server.handler;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.matflis.parking.viewer.server.service.SubscriptionService;
import pl.matflis.parkingweb.core.subscriptions.model.ChargeInfo;
import pl.matflis.parkingweb.core.util.SerDeMapper;
import ratpack.handling.Context;
import ratpack.handling.Handler;

import java.util.Arrays;
import java.util.List;

public class GetChargesHandler implements Handler {

	private static final Logger logger = LoggerFactory.getLogger(GetChargesHandler.class);

	private static final List<Pair<String, String>> HEADERS = Arrays.asList(
			Pair.of("Access-Control-Allow-Origin", "*"), //
			Pair.of("Content-type", "application/json") //
	);

	private final SubscriptionService subscriptionService = SubscriptionService.getInstance();

	@Override
	public void handle(Context ctx) throws Exception {
		logger.debug("Getting charges");
		HEADERS.forEach(header -> {
			ctx.getResponse().getHeaders()
					.add(header.getLeft(), header.getRight()); //
		});
		List<ChargeInfo> charges = subscriptionService.getCharges();
		ctx.render(SerDeMapper.toJson(charges));
		logger.debug("Charges sent!");
	}
}