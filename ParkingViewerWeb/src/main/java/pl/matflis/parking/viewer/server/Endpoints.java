package pl.matflis.parking.viewer.server;

final class Endpoints {

	static final String SUBSCRIPTIONS_BASE = "subscriptions";
	static final String CHARGES = "charges";

	private Endpoints() {
	}
}