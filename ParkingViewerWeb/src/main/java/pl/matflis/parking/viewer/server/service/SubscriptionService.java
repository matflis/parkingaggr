package pl.matflis.parking.viewer.server.service;

import pl.matflis.parking.viewer.server.db.ChargeDao;
import pl.matflis.parkingweb.core.subscriptions.model.ChargeInfo;

import java.util.List;

public final class SubscriptionService {

	private static final SubscriptionService Instance = new SubscriptionService();

	private final ChargeDao chargeDao = ChargeDao.getInstance();

	private SubscriptionService() {
	}

	public static SubscriptionService getInstance() {
		return Instance;
	}

	public List<ChargeInfo> getCharges() {
		return chargeDao.getCharges();
	}
}