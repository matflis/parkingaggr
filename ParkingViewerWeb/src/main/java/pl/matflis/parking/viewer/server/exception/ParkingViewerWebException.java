package pl.matflis.parking.viewer.server.exception;

public class ParkingViewerWebException extends RuntimeException {

	private final int httpStatus;

	protected ParkingViewerWebException(int httpStatus) {
		this.httpStatus = httpStatus;
	}

	public int getHttpStatus() {
		return httpStatus;
	}
}