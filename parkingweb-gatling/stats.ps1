$itNum = 101

FOR($i=1; $i -lt $itNum;$i++) {
	gradle gatlingRun-pl.matflis.parkingweb.testing.ChargeSimulation > outs\output$i.txt
	echo "iteracja nr $i"
}
$best=0;
$acceptable=0;
$extended=0;
$failed=0;

FOR($j=1; $j -lt $itNum;$j++) {
	Select-String -Pattern "> t < 800" -Path outs\output$j.txt | % { $_.Line } > founds\find-best$j.txt
	$best_tmp = Select-String -Path founds\find-best$j.txt -Pattern "\(.*\)" -AllMatches | % { $_.Matches } | % { $_.Value } | % { $_.Substring(1 ,$_.IndexOf("%") - 1) }
	$best = $best + [int]::Parse($best_tmp)
	
	Select-String -Pattern "> 800 ms < t" -Path outs\output$j.txt | % { $_.Line } > founds\find-acceptable$j.txt
	$acceptable_tmp = Select-String -Path founds\find-acceptable$j.txt -Pattern "\(.*\)" -AllMatches | % { $_.Matches } | % { $_.Value } | % { $_.Substring(1 ,$_.IndexOf("%") - 1) }
	$acceptable = $acceptable + [int]::Parse($acceptable_tmp)

	Select-String -Pattern "> t > 1200" -Path outs\output$j.txt | % { $_.Line } > founds\find-extended$j.txt
	$extended_tmp = Select-String -Path founds\find-extended$j.txt -Pattern "\(.*\)" -AllMatches | % { $_.Matches } | % { $_.Value } | % { $_.Substring(1 ,$_.IndexOf("%") - 1) }
	$extended = $extended + [int]::Parse($extended_tmp)

	Select-String -Pattern "> failed" -Path outs\output$j.txt | % { $_.Line } > founds\find-failed$j.txt
	$failed_tmp = Select-String -Path founds\find-failed$j.txt -Pattern "\(.*\)" -AllMatches | % { $_.Matches } | % { $_.Value } | % { $_.Substring(1 ,$_.IndexOf("%") - 1) }
	$failed = $failed + [int]::Parse($failed_tmp)
	echo "Parsowalem nr $j"
}
echo ($best / ($itNum - 2))
echo ($acceptable / ($itNum - 2))
echo ($extended/($itNum - 2))
echo ($failed/($itNum - 2))