package pl.matflis.parkingweb.testing

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.http.request.builder.HttpRequestBuilder

class ChargeSimulation extends Simulation {
  val httpConf = http.baseURL("http://localhost:5050/")

  private val builder: HttpRequestBuilder = http("charge")
    .post("subscriptions/charge")
    .header("Content-Type", "application/json")
    .body(buildBody)

  private def buildBody = {
    StringBody(
      """
        |{
        |  "userName": "admin",
        |  "subscriptionEntryInfo": {
        |   "subscriptionDTO": {
        |     "streetName": "Racławicka",
        |     "registrationNumber": "PLDB 123",
        |     "countryCode": "PL",
        |     "brand": "Mazda",
        |     "remarks": "Stary pojazd",
        |     "violationType": "Brak biletu",
        |     "ticketAmount": 100
        |   },
        |    "offenceTime": "2015-01-01T15:00:00.000Z"
        |  },
        |  "position": {
        |    "longitude": 20,
        |    "latitude": 20
        |  }
        |}
      """.stripMargin)
  }

  val scn = scenario("Charge") //
    .exec(builder)

  setUp(
    scn.inject(atOnceUsers(1400))
  ).protocols(httpConf)
}
