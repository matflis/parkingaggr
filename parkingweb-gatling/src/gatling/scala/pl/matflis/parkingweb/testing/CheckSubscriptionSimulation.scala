package pl.matflis.parkingweb.testing

import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._
import io.gatling.http.request.builder.HttpRequestBuilder

class CheckSubscriptionSimulation extends Simulation {
  val httpConf = http.baseURL("http://localhost:5050/")

  private val builder: HttpRequestBuilder = http("check_subscriptions")
    .post("subscriptions/check")
    .header("Content-Type", "application/json")
    .body(StringBody("{\n  \"streetName\": \"Bronowicka\",\n  \"registrationNumber\": \"ABC 123\"\n}"))

  val scn = scenario("Check Subscriptions") //
    .exec(builder)

  setUp(
    scn.inject(atOnceUsers(1800))
  ).protocols(httpConf)
}